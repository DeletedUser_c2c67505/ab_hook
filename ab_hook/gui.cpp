#include "gui.hpp"

cGuiInstance::cGuiInstance()
{
	m_Font = g_surface->CreateFont();
	g_surface->SetFontGlyphSet(m_Font, "Tahoma", 16, FW_BOLD, 0, 0, FONTFLAG_OUTLINE);
}

void cGuiInstance::Begin(bool bMenuFilling)
{
	if (!m_bFocused)
	{
		const bool bDownClicked = WasClicked(eArrowKey_Down);
		const bool bUpClicked = WasClicked(eArrowKey_Up);

		if (bUpClicked && !bDownClicked && !IsDown(eArrowKey_Down))
			--m_iCurrentVertical[m_iCurrentHorizontal];

		if (bDownClicked && !bUpClicked && !IsDown(eArrowKey_Up))
			++m_iCurrentVertical[m_iCurrentHorizontal];
	}

	int iMaxVertical = 0;

	for (short i = 0; i < m_iMaxColumns; i++)
	{
		if (m_iCurrentVertical[i] >= m_iVerticalIndex[i])
			m_iCurrentVertical[i] = m_iVerticalBegin[i];

		if (m_iCurrentVertical[i] < m_iVerticalBegin[i])
			m_iCurrentVertical[i] = m_iVerticalIndex[i] - 1;

		if (iMaxVertical < m_iVerticalIndex[i])
			iMaxVertical = m_iVerticalIndex[i];

		m_iVerticalIndex[i] = 0;
	}

	if (m_iCurrentHorizontal > m_iMaxColumns)
		m_iCurrentHorizontal = m_iMaxColumns - 1;

	if (m_iCurrentHorizontal < 0)
		m_iCurrentHorizontal = 0;

	if (bMenuFilling)
	{
		g_surface->DrawSetColor(20, 20, 20, 200);

		g_surface->DrawFilledRect(
			m_iPosX - 8,
			m_iPosY - 4,
			m_iPosX - 8 + (m_iCurrentHorizontal + 1) * m_iColumnsPadding + 16,
			m_iPosY - 4 + iMaxVertical * m_iVerticalPadding + 8);
	}

	g_surface->DrawSetTextFont(m_Font);
}

void cGuiInstance::End()
{
	for (int i = 0; i < eArrowKey_Size; i++)
		WasClicked((eArrowKey)i);
}

bool cGuiInstance::OnWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	bool key_down = msg == WM_KEYDOWN;

	if (key_down || msg == WM_KEYUP)
	{
		int index = wParam - VK_LEFT;

		if (index >= eArrowKey_Left && index <= eArrowKey_Down)
		{
			m_pKeysState[index] <<= 1;

			BIT_SET(m_pKeysState[index], BUTTON_CURRENT_STATE, key_down);

			return true;
		}
	}

	return false;
}

bool cGuiInstance::Tab(const wstring strText)
{
	Element_t* pElement = UpdateElement(strText, eElementType::Tab);

	if (pElement->m_bActive)
	{
		if (m_iCurrentHorizontal == pElement->m_iHorizontalPos && WasClicked(eArrowKey_Right))
		{
			m_iCurrentVertical[pElement->m_iHorizontalPos + 1] = pElement->m_iVerticalPos;
			m_iVerticalBegin[pElement->m_iHorizontalPos + 1] = pElement->m_iVerticalPos;

			++m_iCurrentHorizontal;
			ResetKeyState(eArrowKey_Right);
		}

		if (!m_bFocused && WasClicked(eArrowKey_Left))
		{
			if (m_iCurrentHorizontal != 0)
				--m_iCurrentHorizontal;

			ResetKeyState(eArrowKey_Left);
		}
	}

	const auto Color = pElement->m_bActive ? (pElement->m_iHorizontalPos == m_iCurrentHorizontal) ? 
		color_t(255,0,255,255) :		/* pink */
		color_t(0, 255, 255, 255) :		/* sky blue*/
		color_t(255, 255, 255, 255);	/* white */


	g_surface->DrawSetTextPos(
		m_iPosX + m_iColumnsPadding * pElement->m_iHorizontalPos,
		m_iPosY + m_iVerticalPadding * pElement->m_iVerticalPos);

	g_surface->DrawSetTextColor(Color);
	g_surface->DrawPrintText(strText.c_str(), GetTextLen(strText));

	const bool bResult = pElement->m_bActive && m_iCurrentHorizontal != pElement->m_iHorizontalPos;

	if (bResult)
	{
		if (m_iCurrentVertical[pElement->m_iHorizontalPos + 1] < pElement->m_iVerticalPos)
			m_iCurrentVertical[pElement->m_iHorizontalPos + 1] = -1;

		m_iVerticalIndex[pElement->m_iHorizontalPos + 1] = pElement->m_iVerticalPos;
	}

	return bResult;
}

bool cGuiInstance::Checkbox(const wstring strText, bool* pBoolean)
{
	Element_t* pElement = UpdateElement(strText, eElementType::Checkbox);

	bool bResult = false;

	if (pElement->m_bActive)
	{
		if (!m_bFocused && WasClicked(eArrowKey_Right))
			m_bFocused = true;

		if (m_bFocused && WasClicked(eArrowKey_Left))
			m_bFocused = false;

		if (m_bFocused && (WasClicked(eArrowKey_Up) || WasClicked(eArrowKey_Down)))
		{
			*pBoolean ^= 1;
			bResult = true;
		}
	}

	const short iVerticalPos = m_iPosY + m_iVerticalPadding * pElement->m_iVerticalPos;
	const short iHorizontalPos = m_iPosX + m_iColumnsPadding * pElement->m_iHorizontalPos;

	g_surface->DrawSetTextColor(
		pElement->m_bActive ? (pElement->m_iHorizontalPos == m_iCurrentHorizontal) && !m_bFocused ?
		color_t(255, 0, 255, 255) :		/* pink */
		color_t(0, 255, 255, 255) :		/* sky blue*/
		color_t(255, 255, 255, 255));	/* white */

	g_surface->DrawSetTextPos(iHorizontalPos, iVerticalPos);

	g_surface->DrawPrintText(strText.c_str(), pElement->n_iTextLen);

	g_surface->DrawSetTextColor(
		pElement->m_bActive && m_bFocused ? 
		color_t(255, 0, 255, 255) : *pBoolean ?	/* pink */
		color_t(0, 255, 0, 255) :				/* green */
		color_t(255, 255, 255, 255));			/* white */

	g_surface->DrawSetTextPos(iHorizontalPos + m_iHorizontalPadding, iVerticalPos);

	g_surface->DrawPrintText(*pBoolean ? L"on " : L"off", 4);


	return bResult;
}

template<typename T>
bool cGuiInstance::SliderUniversal(const wstring strText, T* pValue, const wchar_t* szFormat, const T Min, const T Max, T Step)
{
	Element_t* pElement = UpdateElement(strText, eElementType::Slider);

	const T iLastValue = *pValue;

	if (pElement->m_bActive)
	{
		if (!m_bFocused && WasClicked(eArrowKey_Right))
			m_bFocused = true;

		if (m_bFocused && WasClicked(eArrowKey_Left))
			m_bFocused = false;

		if (m_bFocused)
		{
			const bool UpPressed = IsDown(eArrowKey_Up);
			const bool DownPressed = IsDown(eArrowKey_Down);

			static auto dwLastPressed = 0;
			static int iCounter = 0;

			if (UpPressed || DownPressed)
			{
				const auto dwTickCount = GetTickCount();

				if (dwTickCount - dwLastPressed > 150 ||
					(WasClicked(eArrowKey_Up) ||
						WasClicked(eArrowKey_Down)))
				{
					if (iCounter)
						Step *= iCounter * 2;

					*pValue +=
						UpPressed ? Step :
						DownPressed ? -Step : 0;

					dwLastPressed = dwTickCount;
					++iCounter;
				}
			}
			else
			{
				iCounter = 0;
			}
		}
	}

	if (*pValue > Max)
		*pValue = Max;

	if (*pValue < Min)
		*pValue = Min;

	const short iVerticalPos = m_iPosY + m_iVerticalPadding * pElement->m_iVerticalPos;
	const short iHorizontalPos = m_iPosX + m_iColumnsPadding * pElement->m_iHorizontalPos;

	g_surface->DrawSetTextColor(
		pElement->m_bActive ? (pElement->m_iHorizontalPos == m_iCurrentHorizontal) && !m_bFocused ?
		color_t(255, 0, 255, 255) :		/* pink */
		color_t(0, 255, 255, 255) :		/* sky blue*/
		color_t(255, 255, 255, 255));	/* white */

	g_surface->DrawSetTextPos(iHorizontalPos, iVerticalPos);

	g_surface->DrawPrintText(strText.c_str(), pElement->n_iTextLen);

	g_surface->DrawSetTextColor(
		pElement->m_bActive && m_bFocused ?
		color_t(255, 0, 255, 255) :		/* pink */
		color_t(255, 255, 255, 255));	/* white */

	g_surface->DrawSetTextPos(iHorizontalPos + m_iHorizontalPadding, iVerticalPos);

	wchar_t buf[16];
	if (auto len = swprintf_s(buf, szFormat, *pValue))if (len != -1)
			g_surface->DrawPrintText(buf, len);
	
	return iLastValue != *pValue;
}

bool cGuiInstance::Combo(const wstring strText, int* pIndex, wstring pStrings[], const int iSize)
{
	Element_t* pElement = UpdateElement(strText, eElementType::Combo);

	const int iLastValue = *pIndex;

	if (pElement->m_bActive)
	{
		if (!m_bFocused && WasClicked(eArrowKey_Right))
			m_bFocused = true;

		if (m_bFocused && WasClicked(eArrowKey_Left))
			m_bFocused = false;

		if (m_bFocused)
		{
			*pIndex +=
				WasClicked(eArrowKey_Up) ? 1 :
				WasClicked(eArrowKey_Down) ? -1 : 0;
		}
	}

	if (*pIndex >= iSize)
		*pIndex = 0;

	if (*pIndex < 0)
		*pIndex = iSize - 1;

	const short iVerticalPos = m_iPosY + m_iVerticalPadding * pElement->m_iVerticalPos;
	const short iHorizontalPos = m_iPosX + m_iColumnsPadding * pElement->m_iHorizontalPos;

	g_surface->DrawSetTextColor(
		pElement->m_bActive ? (pElement->m_iHorizontalPos == m_iCurrentHorizontal) && !m_bFocused ?
		color_t(255, 0, 255, 255) :		/* pink */
		color_t(0, 255, 255, 255) :		/* sky blue*/
		color_t(255, 255, 255, 255));	/* white */


	g_surface->DrawSetTextPos(iHorizontalPos, iVerticalPos);
	g_surface->DrawPrintText(strText.c_str(), pElement->n_iTextLen);

	g_surface->DrawSetTextColor(
		pElement->m_bActive && m_bFocused ?
		color_t(255, 0, 255, 255) :		/* pink */
		color_t(255, 255, 255, 255));	/* white */

	const int pos_x = iHorizontalPos + m_iHorizontalPadding;
	g_surface->DrawSetTextPos(pos_x, iVerticalPos);
	g_surface->DrawPrintText(pStrings[*pIndex].c_str(), pStrings[*pIndex].size());

	if (pElement->m_bActive && m_bFocused)
	{
		const int neg = *pIndex == 0 ? iSize - 1 : *pIndex - 1;
		const int pos = *pIndex == iSize - 1 ? 0 : *pIndex + 1;

		g_surface->DrawSetTextColor(255, 255, 255, 150);

		g_surface->DrawSetTextPos(pos_x, iVerticalPos - m_iVerticalPadding / 2);
		g_surface->DrawPrintText(pStrings[pos].c_str(), pStrings[pos].size());

		g_surface->DrawSetTextPos(pos_x, iVerticalPos + m_iVerticalPadding / 2);
		g_surface->DrawPrintText(pStrings[neg].c_str(), pStrings[neg].size());
	}

	return iLastValue != *pIndex;
}

bool cGuiInstance::ColorPicker(const wstring text, color_t* color_ptr)
{
	constexpr int size = 8;

	wstring color_names[size] =
	{
		L"black",
		L"white",

		L"red",
		L"green",
		L"blue",

		L"yellow",
		L"pink",
		L"sky_blue"
	};

	const color_t colors[size] =
	{
	color_t(0x0, 0x0, 0x0),
	color_t(0xFF, 0xFF, 0xFF),

	color_t(0xFF, 0x0, 0x0),
	color_t(0x0, 0xFF, 0x0),
	color_t(0x0, 0x0, 0xFF),

	color_t(0xFF, 0xFF, 0x0),
	color_t(0xFF, 0x0, 0xFF),
	color_t(0x0, 0xFF, 0xFF)
	};

	int i = 0;

	for (; i != size; i++)
		if (*color_ptr == colors[i])
			break;

	if (i != size && this->Combo(text, &i, color_names, size))
	{
		*color_ptr = colors[i];
		return true;
	}

	return false;
}

bool cGuiInstance::WasClicked(eArrowKey key)
{
	const bool ret =
		(m_pKeysState[key] & BUTTON_PREVIOUS_STATE) &&
		!(m_pKeysState[key] & BUTTON_CURRENT_STATE);

	if (ret)
		ResetKeyState(key);

	return ret;
}

bool cGuiInstance::IsDown(eArrowKey key)
{
	return m_pKeysState[key] & BUTTON_CURRENT_STATE;
}

void cGuiInstance::ResetKeyState(eArrowKey key)
{
	m_pKeysState[key] = 0;
}

Element_t* cGuiInstance::UpdateElement(const wstring& strText, const eElementType Type)
{
	Element_t* pElement = nullptr;

	auto Element = m_pElementsList.find(strText);

	if (Element == m_pElementsList.end())
	{
		pElement = new Element_t(Type, m_iCurrentHorizontal, m_iVerticalIndex[m_iCurrentHorizontal], GetTextLen(strText));
		m_pElementsList.insert(pair<wstring, Element_t*>(strText, pElement));
	}
	else
	{
		pElement = (*Element).second;
	}

	pElement->m_bActive = m_iCurrentVertical[pElement->m_iHorizontalPos] == pElement->m_iVerticalPos;

	++m_iVerticalIndex[pElement->m_iHorizontalPos];

	return pElement;
}

template bool cGuiInstance::SliderUniversal<int>(const wstring, int*, const wchar_t*, const int, const int, int);