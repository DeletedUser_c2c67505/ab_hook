#pragma once

#include <vector>
#include <memory.h>

#include "sdk/color.hpp"

class settings_t
{
public:
	struct /* esp */
	{
		bool enable = false;

		bool skeleton = false;
		bool name = false;
		bool hp_bar = false;
		bool hp_text = false;
		bool boxes = false;
		bool weapon_name = false;
		bool weapon_ammo = false;
	}
	esp;

	struct /* chams */
	{
		bool enable = false;
		
		bool ingnore_z = true;
		color_t visible_color = color_t(0, 255, 255);
	}
	chams;

	struct /* misc */
	{
		bool bunnyhop = false;
		bool autostrafer = false;
		bool remove_crosshair_circle = false;

		struct
		{
			bool play_sound = false;
			bool show_marker = false;
			bool show_hp = false;
		}
		hitmarker;
	}
	misc;
};
