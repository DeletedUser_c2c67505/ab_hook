#include "hooks.hpp"

#include <windows.h>

#include "../minhook/include/MinHook.h"

#include "sdk/utils.hpp"
#include "logging.hpp"
#include "constants.hpp"

#include "features/esp.hpp"
#include "features/misc.hpp"
#include "features/chams.hpp"
#include "features/hitmarker.hpp"
#include "features/aimbot.hpp"
#include "features/menu.hpp"

namespace hooks
{
	using edx = void*;

	using create_move_t = bool(__thiscall*)(void*, float, user_cmd_t*);
	create_move_t o_create_move;

	using paint_traverse_t = void*(__thiscall*)(void*, VPANEL panel, bool forceRepaint, bool allowForce);
	paint_traverse_t o_paint_traverse;

	draw_model_execute_t o_draw_model_execute;

	WNDPROC o_wndproc = nullptr;

	bool __fastcall hook_create_move(void* thisptr, edx, float input_sample_time, user_cmd_t* cmd)
	{
		features::misc::on_create_move(input_sample_time, cmd);

		return(o_create_move(thisptr, input_sample_time, cmd));
	}

	void* __fastcall hook_draw_model_execute(void* thisptr, edx, DrawModelState_t* state,
		ModelRenderInfo_t* pInfo, float* pCustomBoneToWorld)
	{
		features::chams::on_draw_model(thisptr, state, pInfo, pCustomBoneToWorld);
		return(o_draw_model_execute(thisptr, state, pInfo, pCustomBoneToWorld));
	}

	void __fastcall hook_paint_traverse(void* thisptr, edx, VPANEL panel, bool forceRepaint, bool allowForce)
	{
		/* store screen size */
		g_engine_client->GetScreenSize(g_screen_w, g_screen_h);

		/* printf("panel: %x | %d | %d | %s\n", panel, int(forceRepaint), int(allowForce), g_panel->GetName(panel)); */
		
		auto vpanel_getter = [panel](VPANEL& find_panel, const char* panel_name)
		{
			if (!find_panel && !strcmp(panel_name, g_panel->GetName(panel)))
				find_panel = panel;
		};

		static VPANEL focus_overlay = 0;
		static VPANEL game_menu_button = 0;
		static VPANEL hud_quick_info = 0;

		vpanel_getter(focus_overlay, "FocusOverlayPanel");
		vpanel_getter(game_menu_button, "GameMenuButton");
		vpanel_getter(hud_quick_info, "HUDQuickInfo");

		if (g_settings.misc.remove_crosshair_circle &&
			panel == hud_quick_info)
			return;

		if (forceRepaint &&
			(panel == focus_overlay && g_engine_client->IsInGame())||
			panel == game_menu_button)
		{
			/* surface rendering */

			features::menu::on_draw();

			//features::aimbot::on_draw();
			features::hitmarker::on_draw();
			features::esp::on_draw();
		}

		o_paint_traverse(thisptr, panel, forceRepaint, allowForce);
	}

	LRESULT WINAPI hook_wndproc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		if (features::menu::on_wndproc(hwnd, msg, wParam, lParam))
			return TRUE;

		return CallWindowProc(o_wndproc, hwnd, msg, wParam, lParam);
	}
	
	bool install()
	{
		auto mh_status = MH_Initialize();
		log_error_code(mh_status != MH_OK, mh_status)

		auto create_move_fn = (void*)tools::scan_pattern(constants::create_move_pattern);

		log_error(!create_move_fn, "failed to find create_move_fn");
		
		mh_status = MH_CreateHook(
			create_move_fn,
			hook_create_move,
			reinterpret_cast<void**>(&o_create_move));
		log_error_code(mh_status != MH_OK, mh_status);

		auto model_render_table = *reinterpret_cast<void***>(g_model_render);

		mh_status = MH_CreateHook(
			model_render_table[constants::draw_model_execute_index],
			hook_draw_model_execute,
			reinterpret_cast<void**>(&o_draw_model_execute));
		log_error_code(mh_status != MH_OK, mh_status);

		auto panel_table = *reinterpret_cast<void***>(g_panel);

		mh_status = MH_CreateHook(
			panel_table[constants::paint_traverse_index],
			hook_paint_traverse,
			reinterpret_cast<void**>(&o_paint_traverse));
		log_error_code(mh_status != MH_OK, mh_status);

		mh_status = MH_EnableHook(MH_ALL_HOOKS);
		log_error_code(mh_status != MH_OK, mh_status);

		o_wndproc = (WNDPROC)SetWindowLong(g_wnd, GWL_WNDPROC, (LONG)hook_wndproc);

		return(true);
	}

	bool uninstall()
	{
		auto mh_status = MH_DisableHook(MH_ALL_HOOKS);
		log_error_code(mh_status != MH_OK, mh_status);

		return(true);
	}
}
