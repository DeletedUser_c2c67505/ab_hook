#pragma once

#include "tools.hpp"

namespace constants
{
	constexpr auto main_window_class_name = "Valve001";
	constexpr auto process_name = "hl2.exe";

	/* patterns list */

	constexpr tools::pattern_info_t create_move_pattern =	{ "client.dll",			"55 8B EC E8 ? ? ? ? 8B C8 85 C9 75 06" };

	/* vmt indexes list */

	constexpr long end_scene_index = 42;
	constexpr long reset_index = 16;
	constexpr long present_index = 17;
	constexpr long draw_model_execute_index = 19;
	constexpr long paint_traverse_index = 41;

	/* interface names list */

	constexpr auto entity_list_interface =			"VClientEntityList003";
	constexpr auto engine_client_interface =		"VEngineClient014";
	constexpr auto model_render_interface =			"VEngineModel016";
	constexpr auto engine_cvar_interface =			"VEngineCvar004";
	constexpr auto material_system_interface =		"VMaterialSystem080";
	constexpr auto render_view_interface =			"VEngineRenderView014";
	constexpr auto model_info_interface =			"VModelInfoClient006";
	constexpr auto surface_interface =				"VGUI_Surface030";
	constexpr auto panel_interface =				"VGUI_Panel009";
	constexpr auto player_info_manager_interface =	"PlayerInfoManager002";
	constexpr auto game_event_manager =				"GAMEEVENTSMANAGER002";
}