#pragma once

#include "math_classes/vector3.hpp"
#include "math_classes/matrix3x4.hpp"
#include "math_classes/vmatrix.hpp"

namespace math
{
	/* pi 4 decimal places */
	constexpr float pi = 3.1415f;

	/* returns squared x */
	__forceinline const float square(const float x);

	/* converts degrees to radians */
	const float deg_to_rad(const float deg);
	/* converts radians to degrees */
	__forceinline const float rad_to_deg(const float rad);
	/* normalizes angle */
	float clamp_deg(float deg);

	/* transforms world coordinates to screen position coefficient */
	bool screen_transform(const vmatrix_t& w2s_matrix, const vector3& in, float& x, float& y);

	void vector_transform(const vector3& in1, const matrix3x4_t& in2, vector3& out);

	/* returns distance between two vectors */
	float distance(const vector3& vec1, const vector3& vec2);
	/* returns angle between two view angles */
	float get_fov(const vector3& ang1, const vector3& ang2);
}