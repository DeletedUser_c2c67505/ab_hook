#pragma once

#include <windows.h>

namespace features
{
	namespace menu
	{
		void initialize();
		bool on_wndproc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
		void on_draw();
	}
}