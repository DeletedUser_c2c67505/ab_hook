#include "misc.hpp"


namespace features
{
	namespace misc
	{
		bool bunnyhop(intptr_t local_player, user_cmd_t* cmd);
		bool autostrafer(intptr_t local_player, user_cmd_t* cmd);

		bool on_create_move(float input_sample_time, user_cmd_t* cmd)
		{
			bool ret = true;

			if (auto local_player = (intptr_t)g_entity_list->GetClientEntity(g_engine_client->GetLocalPlayer()))
			{
				ret &= autostrafer(local_player, cmd);
				ret &= bunnyhop(local_player, cmd);
			}

			return(ret);
		}

		bool bunnyhop(intptr_t local_player, user_cmd_t* cmd)
		{
			if (g_settings.misc.bunnyhop &&
				(cmd->buttons & IN_JUMP) &&
				!(*(int*)(local_player + 0x350) & FL_ONGROUND)) /* 0x350 - m_fFlags offset */
			{
				cmd->buttons &= ~IN_JUMP;
			}

			return(true);
		}

		bool autostrafer(intptr_t local_player, user_cmd_t* cmd)
		{
			if (g_settings.misc.autostrafer &&
				cmd->buttons & IN_JUMP)
			{
				if (cmd->mousedx > 1 || cmd->mousedx < -1)
				{
					cmd->side_move = cmd->mousedx < 0.f ? -450.f : 450.f;
				}
				else
				{
					cmd->forward_move = 5850.f / math::distance(*(vector3*)(local_player + 0xf4), vector3());
					cmd->side_move = (cmd->command_number % 2) == 0 ? -450.f : 450.f;

					if (cmd->forward_move > 450.f)
						cmd->forward_move = 450.f;
				}
			}

			return(true);
		}
	}
}
