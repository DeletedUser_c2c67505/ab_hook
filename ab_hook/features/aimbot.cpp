#include "aimbot.hpp"

#include <stdio.h>

#include "../sdk/sdk.hpp"
#include "../sdk/utils.hpp"
#include "../globals.hpp"

namespace features
{
	namespace aimbot
	{
		vector3 get_bone_position(unsigned entity, int bone)
		{
			auto vec_result = vector3();
			matrix3x4_t matrix_arr[128];

			auto renderable_ptr = (void*)(entity + 0x4);

			def_virtual(bool)(void*, matrix3x4_t*, int, int, float);
			if (!get_virtual(original_fn, renderable_ptr, 16)(renderable_ptr, matrix_arr, 128, 0x00000100, g_global_vars->realtime))
				return vec_result;

			matrix3x4_t HitboxMatrix = matrix_arr[bone];

			vec_result = vector3(HitboxMatrix[0][3], HitboxMatrix[1][3], HitboxMatrix[2][3]);

			return vec_result;
		}

		void on_draw()
		{
			auto local_player_index = g_engine_client->GetLocalPlayer();
			auto local_player = (unsigned)g_entity_list->GetClientEntity(local_player_index);

			if (!local_player)
				return;

			for (int i = 1; i < 4096; i++)
			{
				if (i == local_player_index)
					return;
				auto player = (unsigned)g_entity_list->GetClientEntity(i);
				if (!player)
					continue;

				auto health = *(int*)(player + 0x94);
				if (health <= 0 || health > 100)
					continue;


				for (int j = 0; j < 15; j++)
				{
					auto bone_pos = get_bone_position(player, j);

					int screen_x, screen_y;

					if (!world_to_screen(bone_pos, screen_x, screen_y))
						continue;

					static unsigned font = 0;

					if (!font)
					{
						font = g_surface->CreateFont();
						g_surface->SetFontGlyphSet(font, "Tahoma", 14, FW_NORMAL, 0, 0, FONTFLAG_OUTLINE);
					}

					wchar_t buf[4];

					const int size = swprintf(buf, L"%d", j);

					if (size != -1)
					{
						int w, h;
						g_surface->GetTextSize(font, buf, w, h);

						g_surface->DrawSetTextColor(50, 200, 50, 255);
						g_surface->DrawSetTextPos((int)screen_x, (int)screen_y);
						g_surface->DrawPrintText(buf, size);
					}
				}
			}
		}
	}
}
