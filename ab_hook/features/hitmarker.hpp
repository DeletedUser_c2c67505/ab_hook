#pragma once

namespace features
{
	namespace hitmarker
	{
		void initialize();
		void on_draw();
	}
}
