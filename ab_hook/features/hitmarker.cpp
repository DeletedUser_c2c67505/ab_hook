#include "hitmarker.hpp"

#include <stdio.h>
#include <windows.h>
#include <mmsystem.h>
#pragma comment(lib, "winmm.lib")

#include "../sdk/event_listener.hpp"
#include "../resources/hitsound.hpp"
#include "../sdk/utils.hpp"

namespace features
{
	namespace hitmarker
	{
		float hit_time = 0.f;
		int hit_health = 0;

		void on_event(IGameEvent* event);

		void initialize()
		{
			new event_listener("player_hurt", on_event);
		}

		void on_event(IGameEvent* event)
		{
			const int attacker_player_index = g_engine_client->GetPlayerForUserID(event->GetInt("attacker"));
			const int local_player_index = g_engine_client->GetLocalPlayer();

			if (attacker_player_index != local_player_index)
				return;
			
			const int victim_index = g_engine_client->GetPlayerForUserID(event->GetInt("userid"));

			if (local_player_index == victim_index)
				return;

			/* hitmarker sound */
			if (g_settings.misc.hitmarker.play_sound)
				PlaySound(reinterpret_cast<LPCSTR>(resources::hitsound), NULL, SND_ASYNC | SND_MEMORY);

			auto entity = (intptr_t)g_entity_list->GetClientEntity(victim_index);

			if (entity)
			{
				hit_time = g_global_vars->realtime;
				hit_health = event->GetInt("health");
			}
		}

		void on_draw()
		{
			constexpr float time_to_show_damage = 1.f;
			constexpr float time_to_show_marker = 0.4f;

			if (g_settings.misc.hitmarker.show_hp &&
				hit_time > g_global_vars->realtime - time_to_show_damage)
			{
				static unsigned font = 0;

				if (!font)
				{
					font = g_surface->CreateFont();
					g_surface->SetFontGlyphSet(font, "Tahoma", 14, FW_NORMAL, 0, 0, FONTFLAG_OUTLINE);
				}
				
				wchar_t buf[4];

				const int size = swprintf(buf, L"%d", hit_health);

				if (size != -1)
				{
					int w, h;
					g_surface->GetTextSize(font, buf, w, h);
					
					g_surface->DrawSetTextColor(50, 200, 50, 255);
					g_surface->DrawSetTextPos(g_screen_w / 2 - w / 2, g_screen_h / 2 - 40);
					g_surface->DrawPrintText(buf, size);
				}
			}
			if (g_settings.misc.hitmarker.show_marker &&
				hit_time > g_global_vars->realtime - time_to_show_marker)
			{
				constexpr int line_lenght = 8;
				constexpr int line_offset = line_lenght / 4;

				const int screen_x = g_screen_w / 2 - 1;
				const int screen_y = g_screen_h / 2;

				g_surface->DrawSetColor(255, 255, 255, 255);

				g_surface->DrawLine(screen_x - line_lenght, screen_y - line_lenght, screen_x - line_offset, screen_y - line_offset);
				g_surface->DrawLine(screen_x - line_lenght, screen_y + line_lenght, screen_x - line_offset, screen_y + line_offset);
				g_surface->DrawLine(screen_x + line_lenght, screen_y + line_lenght, screen_x + line_offset, screen_y + line_offset);
				g_surface->DrawLine(screen_x + line_lenght, screen_y - line_lenght, screen_x + line_offset, screen_y - line_offset);
			}
		}
	}
}	