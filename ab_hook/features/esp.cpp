#include "esp.hpp"

#include <stdio.h>
#include <math.h>

#include "../tools.hpp"
#include "../globals.hpp"
#include "../sdk/utils.hpp"
#include "../math.hpp"

namespace features
{
	namespace esp
	{
		bool get_bounding_box( unsigned entity, float& x, float& y, float& height, float& width )
		{
			matrix3x4_t trans = *( matrix3x4_t* )(entity + 0x308);

			vector3 min = *( vector3* )(entity + 0x1A4);
			vector3 max = *( vector3* )(entity + 0x1B0);

			vector3 points[] = { vector3( min.x, min.y, min.z ),
							vector3( min.x, max.y, min.z ),
							vector3( max.x, max.y, min.z ),
							vector3( max.x, min.y, min.z ),
							vector3( max.x, max.y, max.z ),
							vector3( min.x, max.y, max.z ),
							vector3( min.x, min.y, max.z ),
							vector3( max.x, min.y, max.z ) };

			vector3 pointsTransformed[8];

			for ( int i = 0; i < 8; i++ )
			{
				math::vector_transform( points[i], trans, pointsTransformed[i] );
			}

			vector3 flb, brt, blb, frt,
				frb, brb, blt, flt;


			if ( !world_to_screen( pointsTransformed[3], flb ) || !world_to_screen( pointsTransformed[5], brt )
				|| !world_to_screen( pointsTransformed[0], blb ) || !world_to_screen( pointsTransformed[4], frt )
				|| !world_to_screen( pointsTransformed[2], frb ) || !world_to_screen( pointsTransformed[1], brb )
				|| !world_to_screen( pointsTransformed[6], blt ) || !world_to_screen( pointsTransformed[7], flt ) )
				return false;

			vector3 arr[] = { flb, brt, blb, frt, frb, brb, blt, flt };

			float left = flb.x;
			float top = flb.y;
			float right = flb.x;
			float bottom = flb.y;

			for ( int i = 1; i < 8; i++ )
			{
				if ( left > arr[i].x )
					left = arr[i].x;
				if ( top < arr[i].y )
					top = arr[i].y;
				if ( right < arr[i].x )
					right = arr[i].x;
				if ( bottom > arr[i].y )
					bottom = arr[i].y;
			}

			height = top - bottom;
			width = right - left;
			x = left; y = bottom;

			return true;
		}

		model_t* GetModel( unsigned entity )
		{
			static auto get_model = tools::scan_pattern( "client.dll", "0F BF 81 ? ? ? ?" );
			return ((model_t*(__thiscall*)(unsigned))get_model)(entity);
		}

		studiohdr_t* GetStudioModel( unsigned entity )
		{
			if ( const auto model = GetModel( entity ) )
				return (g_model_info->GetStudioModel( model ));

			return(nullptr);
		}

		mstudiohitboxset_t* GetHitBoxSet( unsigned entity )
		{
			studiohdr_t* pStudioModel = nullptr;
			mstudiohitboxset_t* pHitboxSet = nullptr;

			pStudioModel = GetStudioModel( entity );

			if ( !pStudioModel )
				return pHitboxSet;

			pHitboxSet = pStudioModel->pHitboxSet( 0 );

			if ( !pHitboxSet )
				return nullptr;

			return pHitboxSet;
		}

		mstudiobbox_t* GetHitBox( unsigned entity, int Hitbox )
		{
			if ( Hitbox < 0 || Hitbox >= 0xabc )
				return nullptr;

			if ( auto pHitboxSet = GetHitBoxSet( entity ) )
				return pHitboxSet->pHitbox( Hitbox );

			return(nullptr);
		}
		vector3 GetHitboxPosition( unsigned entity, int nHitbox )
		{
			matrix3x4_t MatrixArray[128];
			vector3 vRet, vMin, vMax;

			vRet = vector3( 0, 0, 0 );

			mstudiobbox_t* pHitboxBox = GetHitBox( entity, nHitbox );

			if ( !pHitboxBox )
				return vRet;

			auto renderable_ptr = ( void* )(entity + 0x4);
			def_virtual( bool )(void*, matrix3x4_t*, int, int, float);
			if ( !get_virtual( original_fn, renderable_ptr, 16 )(renderable_ptr, MatrixArray, 128, 0x00000100, 0) )
				return vRet;

			if ( !pHitboxBox->m_Bone ||
				!pHitboxBox->m_vBbmin.is_valid( ) ||
				!pHitboxBox->m_vBbmax.is_valid( ) )
				return vRet;

			math::vector_transform( pHitboxBox->m_vBbmin, MatrixArray[pHitboxBox->m_Bone], vMin );
			math::vector_transform( pHitboxBox->m_vBbmax, MatrixArray[pHitboxBox->m_Bone], vMax );

			vRet = (vMin + vMax) * 0.5f;

			return vRet;
		}

		vector3 get_bone_position( unsigned entity, int bone )
		{
			auto vec_result = vector3( );
			matrix3x4_t matrix_arr[128];

			auto renderable_ptr = ( void* )(entity + 0x4);

			def_virtual( bool )(void*, matrix3x4_t*, int, int, float);
			if ( !get_virtual( original_fn, renderable_ptr, 16 )(renderable_ptr, matrix_arr, 128, 0x00000100, g_global_vars->realtime) )
				return vec_result;

			matrix3x4_t HitboxMatrix = matrix_arr[bone];

			vec_result = vector3( HitboxMatrix[0][3], HitboxMatrix[1][3], HitboxMatrix[2][3] );

			return vec_result;
		}

		void draw_bone( unsigned pEntity, const int* iBones, int count, color_t clrCol )
		{
			for ( int i = 0; i < count - 1; i++ )
			{
				auto bone_begin = get_bone_position( ( unsigned )pEntity, iBones[i] );
				auto bone_end = get_bone_position( ( unsigned )pEntity, iBones[i + 1] );

				int begin_x, begin_y, end_x, end_y;

				if ( !world_to_screen( bone_begin, begin_x, begin_y ) ||
					!world_to_screen( bone_end, end_x, end_y ) )
					continue;

				g_surface->DrawSetColor( clrCol.r, clrCol.g, clrCol.b, clrCol.a );
				g_surface->DrawLine(
					static_cast< int >(begin_x),
					static_cast< int >(begin_y),
					static_cast< int >(end_x),
					static_cast< int >(end_y) );
			}
		}

		void draw_box( unsigned player, int index )
		{
			if ( g_settings.esp.skeleton )
			{
				static const int iLeftArmBones[] = { 13, 16, 17, 18 };
				static const int iRightArmBones[] = { 13, 29, 30, 31 };
				static const int iHeadBones[] = { 9, 10, 11, 12, 13, 14 };
				static const int iLeftLegBones[] = { 9, 1, 2, 3 };
				static const int iRightLegBones[] = { 7, 6, 5, 9 };

				const auto white_color = color_t( );

				draw_bone( player, iLeftArmBones, 4, white_color );
				draw_bone( player, iRightArmBones, 4, white_color );

				draw_bone( player, iHeadBones, 6, white_color );

				draw_bone( player, iLeftLegBones, 4, white_color );
				draw_bone( player, iRightLegBones, 4, white_color );
			}


			//vector3 vec_head = get_bone_position(player, 14);

			constexpr int hitbox_head = 1;
			constexpr int hitbox_left_foot = 11;
			constexpr int hitbox_right_foot = 14;


			vector3 vec_head = GetHitboxPosition( player, hitbox_head );
			vec_head.z += 5.f;

			vector3 vec_pos = *( vector3* )(player + 0x338);


			auto local_player = ( unsigned )g_entity_list->GetClientEntity( g_engine_client->GetLocalPlayer( ) );

			if ( !local_player )
				return;

			const float angle_y = *( float* )(player + 0x348);

			constexpr float angle_diff = 90.f;
			constexpr float box_half = 14.f;

			const float rad_left_ang = math::deg_to_rad( math::clamp_deg( angle_y - angle_diff ) );
			const float rad_right_ang = math::deg_to_rad( math::clamp_deg( angle_y + angle_diff ) );

			const vector3 vec_left_foot = vector3( vec_head.x + box_half * cosf( rad_left_ang ), vec_head.y + box_half * sinf( rad_left_ang ), vec_pos.z );
			const vector3 vec_right_foot = vector3( vec_head.x + box_half * cosf( rad_right_ang ), vec_head.y + box_half * sinf( rad_right_ang ), vec_pos.z );

			int head_x, head_y,
				left_foot_x, left_foot_y,
				right_foot_x, right_foot_y;

			if ( !world_to_screen( vec_head, head_x, head_y ) ||
				!world_to_screen( vec_left_foot, left_foot_x, left_foot_y ) ||
				!world_to_screen( vec_right_foot, right_foot_x, right_foot_y ) )
				return;

			const int pos_y = max( left_foot_y, right_foot_y );

			float x, y, h, w;

			get_bounding_box( player, x, y, h, w );

			const auto box_height = static_cast<int>(h);
			const auto box_width = static_cast< int >(w);

			const auto box_screen_x = static_cast< int >(x);
			const auto box_screen_y = static_cast< int >(y);

			

			if ( g_settings.esp.boxes )
			{
				g_surface->DrawSetColor( 255, 0, 0, 255 );
				g_surface->DrawOutlinedRect(
					box_screen_x,
					box_screen_y,
					box_screen_x + box_width,
					box_screen_y + box_height );

				g_surface->DrawSetColor( 0, 0, 0, 255 );
				g_surface->DrawOutlinedRect(
					box_screen_x - 1,
					box_screen_y - 1,
					box_screen_x + box_width + 1,
					box_screen_y + box_height + 1 );

				g_surface->DrawSetColor( 0, 0, 0, 255 );
				g_surface->DrawOutlinedRect(
					box_screen_x + 1,
					box_screen_y + 1,
					box_screen_x + box_width - 1,
					box_screen_y + box_height - 1 );
			}

			static unsigned font = 0;

			if ( !font )
			{
				font = g_surface->CreateFont( );
				g_surface->SetFontGlyphSet( font, "Tahoma", 14, FW_NORMAL, 0, 0, FONTFLAG_OUTLINE );
			}

			g_surface->DrawSetTextFont( font );

			if ( g_settings.esp.name )
			{
				player_info_t player_info;
				g_engine_client->GetPlayerInfo( index, &player_info );

				wchar_t buf[MAX_PLAYER_NAME_LENGTH];

				const int len = strlen( player_info.name );

				int needed = MultiByteToWideChar( CP_UTF8, 0, player_info.name, len + 1, buf, len + 1 );
				int text_width, text_height;

				g_surface->GetTextSize( font, buf, text_width, text_height );

				g_surface->DrawSetTextColor( 255, 255, 255, 255 );
				g_surface->DrawSetTextPos( box_screen_x + box_width / 2 - text_width / 2, box_screen_y - text_height );
				g_surface->DrawPrintText( buf, needed );
			}

			if ( g_settings.esp.hp_bar )
			{
				const auto health = *( int* )(player + 0x94);

				const auto hp_bar_x = box_screen_x + box_width + 8;

				const auto hp_bar_end_y = static_cast< int >(pos_y);
				const auto hp_bar_begin_y = hp_bar_end_y - static_cast< int >(box_height * (health / 100.f));

				g_surface->DrawSetColor( 0, 0, 0, 255 );
				g_surface->DrawOutlinedRect(
					hp_bar_x - 1,
					hp_bar_begin_y + 1,
					hp_bar_x + 3,
					hp_bar_end_y - 2 );

				g_surface->DrawSetColor( 0, 255, 0, 255 );
				g_surface->DrawFilledRect( hp_bar_x, hp_bar_begin_y, hp_bar_x + 2, hp_bar_end_y );


				struct wchar_hp_cache_t
				{
					struct
					{
						wchar_t text[4];
						int text_width;
						int text_len;
					}
					cache[100];

					wchar_hp_cache_t( )
					{
						int h;
						for ( int i = 0; i < 100; i++ )
						{
							cache[i].text_len = swprintf( cache[i].text, L"%d", i + 1 );
							g_surface->GetTextSize( font, cache[i].text, cache[i].text_width, h );
						}
					}
				};

				static auto hp_cache = wchar_hp_cache_t( );

				const auto& cell = hp_cache.cache[health - 1];

				g_surface->DrawSetTextColor( 255, 0, 0, 255 );
				g_surface->DrawSetTextPos( hp_bar_x - cell.text_width / 2 + 1, hp_bar_begin_y - 14 );
				g_surface->DrawPrintText( cell.text, cell.text_len );
			}

			if ( g_settings.esp.weapon_name )
			{
				if ( auto weapon_entity = ( unsigned )g_entity_list->GetClientEntity( *( int* )(player + 0xdf8) & 0xFFF ) )
				{
					if ( auto model = GetModel( weapon_entity ) )
					{
						if ( auto model_name = g_model_info->GetModelName( model ) )
						{
							/* skip "models/weapons/v_" */
							model_name += 17;

							struct weapon_info_t
							{
								union
								{
									char model[4];
									unsigned bynary;
								};

								wchar_t user_friendly[12];
								int user_friendly_len;

								/* "constructor" for constexpr support */
#define node(model_name, name) \
							{ \
								{ \
									model_name[0], \
									model_name[1], \
									model_name[2], \
									model_name[3] \
								}, \
								L##name, \
								(sizeof(name) / sizeof(*name) - 1) \
							}
							}; /* 0x20 */

							constexpr weapon_info_t weapons_list[15] =
							{
								node( "crow", "crowbar" ),
								node( "stun", "baton" ),
								node( "Phys", "gravity gun" ),
								node( "phys", "gravity gun" ),
								node( "pist", "pistol" ),
								node( "357.", "revolver" ),
								node( "smg1", "smg" ),
								node( "irif", "rifle" ),
								node( "shot", "shotgun" ),
								node( "cros", "crossbow" ),
								node( "gren", "grenade" ),
								node( "rpg.", "rpg" ),
								node( "rock", "rpg" ),
								node( "slam", "mine" ),

								node( "XXXX", "unknown" )	/* nothing else found */
							};
#undef node
							int i = -1;

							while (
								++i < arr_size( weapons_list ) - 1 &&
								weapons_list[i].bynary != *( unsigned* )model_name );

							int text_width, text_height;
							g_surface->GetTextSize( font, weapons_list[i].user_friendly, text_width, text_height );
							g_surface->DrawSetTextColor( 255, 255, 255, 255 );
							g_surface->DrawSetTextPos( box_screen_x + box_width / 2 - text_width / 2, box_screen_y + box_height + 10 );

							g_surface->DrawPrintText( weapons_list[i].user_friendly, weapons_list[i].user_friendly_len );


							if ( g_settings.esp.weapon_ammo )
							{
								if ( auto ammo = *( int* )(weapon_entity + 0x8bc); ammo > 0 )
								{
									struct wchar_ammo_cache_t
									{
										struct
										{
											union
											{
												wchar_t text[4];
												long long unsigned binary;
											};

											int text_len;
										}
										cache[45];

										wchar_ammo_cache_t( )
										{
											for ( int i = 0; i < arr_size( cache ); i++ )
											{
												union
												{
													wchar_t buf[8];
													uint64_t binary;
												}
												temp_buf;

												cache[i].text_len = swprintf( temp_buf.buf, L"(%d)", i + 1 );
												cache[i].binary = temp_buf.binary;
											}
										}
									};

									static auto ammo_count_cache = wchar_ammo_cache_t( );
									auto& cell = ammo_count_cache.cache[ammo - 1];

									g_surface->DrawSetTextPos( box_screen_x + box_width / 2 - text_width / 2 + text_width + 2, box_screen_y + box_height + 10 );
									g_surface->DrawPrintText( cell.text, cell.text_len );
								}

							}
						}

					}
				}
			}
		}

		void on_draw( )
		{
			auto local_player_index = g_engine_client->GetLocalPlayer( );
			auto local_player = ( unsigned )g_entity_list->GetClientEntity( local_player_index );

			if ( !local_player )
				return;

			for ( int i = 1; i < 64; i++ )
			{
#ifndef _DEBUG
				if ( i == local_player_index )
					return;
#endif

				auto player = ( unsigned )g_entity_list->GetClientEntity( i );

				if ( !player )
					continue;

				const auto& health = *( int* )(player + 0x94);

				if ( health <= 0 || health > 100 )
					continue;

				draw_box( player, i );
			}
		}
	}
}