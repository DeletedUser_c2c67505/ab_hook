#include "chams.hpp"

namespace features
{
	namespace chams
	{
		intptr_t pLocal = 0;

		IMaterial* matHands = nullptr;
		IMaterial* matInVisible = nullptr;
		IMaterial* matVisible = nullptr;

		bool is_initialized = false;

		MaterialManger chams_materials;

		void ChangePlayers(void* ctx, const DrawModelState_t* state,
			const ModelRenderInfo_t* pInfo, float* pCustomBoneToWorld);

		void Update()
		{
			pLocal = (intptr_t)g_entity_list->GetClientEntity(g_engine_client->GetLocalPlayer());

			if (!pLocal)
				return;

			matHands = nullptr;

			matVisible = chams_materials.GetMaterial(chams_type_t::default, false);
			matInVisible = chams_materials.GetMaterial(chams_type_t::default, true);
		}

		bool NeedOverride = false;

		void on_draw_model(void* ctx, const DrawModelState_t* state, const ModelRenderInfo_t* info, float* customBoneToWorld)
		{
			char* name = (char*)g_model_info->GetModelName(info->pModel);
			if (!is_initialized)
			{
				chams_materials.Init();
				is_initialized = true;
			}

			if (!info->pModel)
				return;

			if (!g_engine_client->IsTakingScreenshot())
			{
				/*printf("%s\n", name);*/
				if (info->entity_index && info->entity_index <= 64 &&
					name[0] == 'm' && (name[7] == 'c' || (name[7] == 'p' && name[7] == 'o') || name[7] == 'h'))
					ChangePlayers(ctx, state, info, customBoneToWorld);
			}

			if (NeedOverride)
				g_model_render->forced_material_override(NULL);
		}

		void ChangePlayers(void* ctx, const DrawModelState_t* state,
			const ModelRenderInfo_t* pInfo, float* pCustomBoneToWorld)
		{
			Update();

			if (!pLocal)
			{
				return;
			}

			intptr_t pEntity = (intptr_t)g_entity_list->GetClientEntity(pInfo->entity_index);

			if (!pEntity)
				return;

			NeedOverride = false;

			constexpr float modifier = 5.f;

			float color[3] =
			{
				1.f + g_settings.chams.visible_color.r * modifier,
				1.f + g_settings.chams.visible_color.g * modifier,
				1.f + g_settings.chams.visible_color.b * modifier
			};

			g_render_view->SetColorModulation(color);

			hooks::o_draw_model_execute(ctx, state, pInfo, pCustomBoneToWorld);
			g_model_render->forced_material_override(NULL);

			NeedOverride = true;
		}
	}
}