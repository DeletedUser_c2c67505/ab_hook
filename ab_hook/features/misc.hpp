#pragma once

#include "../math.hpp"
#include "../globals.hpp"

#include "chams.hpp"
#include "hitmarker.hpp"


namespace features
{
	namespace misc
	{
		bool on_create_move(float input_sample_time, user_cmd_t* cmd);
	}
}
