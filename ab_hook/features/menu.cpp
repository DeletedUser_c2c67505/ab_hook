#include "menu.hpp"

#include "../sdk/utils.hpp"
#include "../gui.hpp"

namespace features
{
	namespace menu
	{
		cGuiInstance* gui_inst = nullptr;

		bool is_menu_open = false;

		void initialize()
		{
			gui_inst = new cGuiInstance;
		}
		bool on_wndproc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
		{
			if (msg == WM_KEYUP && wParam == VK_INSERT)
			{
				is_menu_open = !is_menu_open;
				return(true);
			}

			if (is_menu_open)
			{
				return(gui_inst->OnWndProc(hwnd, msg, wParam, lParam));
			}
			
			return(false);
		}
		void on_draw()
		{
			if (!is_menu_open)
				return;

			static bool fill_menu = false;
			gui_inst->Begin(fill_menu);

			if (gui_inst->Tab(L"esp"))
			{
				gui_inst->Checkbox(L"box#1", &g_settings.esp.boxes);
				gui_inst->Checkbox(L"enable#esp", &g_settings.esp.enable);
				gui_inst->Checkbox(L"box#2", &g_settings.esp.boxes);
				gui_inst->Checkbox(L"skeleton", &g_settings.esp.skeleton);
				gui_inst->Checkbox(L"name", &g_settings.esp.name);
				gui_inst->Checkbox(L"hp", &g_settings.esp.hp_bar);
				gui_inst->Checkbox(L"weapon", &g_settings.esp.weapon_name);
				gui_inst->Checkbox(L"ammo", &g_settings.esp.weapon_ammo);
			}
			if (gui_inst->Tab(L"chams"))
			{
				gui_inst->Checkbox(L"enable#chams", &g_settings.chams.enable);
				gui_inst->Checkbox(L"ignore z", &g_settings.chams.ingnore_z);
				gui_inst->ColorPicker(L"visible color", &g_settings.chams.visible_color);
			}
			if (gui_inst->Tab(L"misc"))
			{
				gui_inst->Checkbox(L"bunnyhop", &g_settings.misc.bunnyhop);
				gui_inst->Checkbox(L"autostrafer", &g_settings.misc.autostrafer);
				gui_inst->Checkbox(L"remove crosshair", &g_settings.misc.remove_crosshair_circle);

				if (gui_inst->Tab(L"hitmarker"))
				{
					gui_inst->Checkbox(L"play sound", &g_settings.misc.hitmarker.play_sound);
					gui_inst->Checkbox(L"show marker", &g_settings.misc.hitmarker.show_marker);
					gui_inst->Checkbox(L"show hp", &g_settings.misc.hitmarker.show_hp);
				}
			}

			if (gui_inst->Tab(L"options"))
			{
				gui_inst->SliderInt(L"position x", &gui_inst->m_iPosX, 0, g_screen_w);
				gui_inst->SliderInt(L"position y", &gui_inst->m_iPosY, 0, g_screen_h);

				gui_inst->Checkbox(L"fill background", &fill_menu);

				std::wstring variants[3] =
				{
					L"first",
					L"second",
					L"third"
				};

				static int index = 0;
				gui_inst->Combo(L"test combo", &index, variants, 3);
			}

			gui_inst->End();
		}
	}
}