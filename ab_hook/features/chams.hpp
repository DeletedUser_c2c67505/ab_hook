#pragma once

#include <fstream>

#include "../globals.hpp"
#include "../hooks.hpp"
#include "../sdk/sdk.hpp"

namespace features
{
	namespace chams
	{
		enum class chams_type_t
		{
			default,
			flat
		};

		void on_draw_model(void* ctx, const DrawModelState_t* state, const ModelRenderInfo_t* info, float* customBoneToWorld);

#define TEXTURE_GROUP_MODEL	"Model textures"

		class MaterialManger
		{
		public:
			MaterialManger()
			{
				CreateDirectory("hl2\\materials", NULL);
				CreateDirectory("hl2\\materials\\vgui", NULL);

				std::ofstream("hl2\\materials\\vgui\\simple_regular.vmt") << R"#("UnlitGeneric"
{
	"$basetexture" "vgui/white_additive"
	"$ignorez"      "0"
	"$envmap"       ""
	"$nofog"        "1"
	"$model"        "1"
	"$nocull"       "0"
	"$selfillum"    "1"
	"$halflambert"  "1"
	"$znearer"      "0"
	"$flat"         "1"
})#";
				std::ofstream("hl2\\materials\\vgui\\simple_ignorez.vmt") << R"#("UnlitGeneric"
{
	"$basetexture" "vgui/white_additive"
	"$ignorez"      "1"
	"$envmap"       ""
	"$nofog"        "1"
	"$model"        "1"
	"$nocull"       "0"
	"$selfillum"    "1"
	"$halflambert"  "1"
	"$znearer"      "0"
	"$flat"         "1"
})#";
			}

			void Init()
			{
				matDefault = g_material_system->FindMaterial("simple_regular", TEXTURE_GROUP_MODEL);
				matDefaultIgnoreZ = g_material_system->FindMaterial("simple_ignorez", TEXTURE_GROUP_MODEL);
				matFlat = g_material_system->FindMaterial("simple_flat", TEXTURE_GROUP_MODEL);
				matFlatIgnoreZ = g_material_system->FindMaterial("simple_flat_ignorez", TEXTURE_GROUP_MODEL);

				matDefaultIgnoreZ->SetMaterialVarFlag(MATERIAL_VAR_IGNOREZ, true);
				matFlatIgnoreZ->SetMaterialVarFlag(MATERIAL_VAR_IGNOREZ, true);

				matFlat->SetMaterialVarFlag(MATERIAL_VAR_FLAT, true);
				matFlatIgnoreZ->SetMaterialVarFlag(MATERIAL_VAR_FLAT, true);
			}

			~MaterialManger()
			{
				std::remove("hl2\\materials\\vgui\\simple_regular.vmt");
				std::remove("hl2\\materials\\vgui\\simple_ignorez.vmt");
			}

			IMaterial* GetMaterial(chams_type_t type, bool ignorez)
			{
				switch (type)
				{
				case chams_type_t::default:
					return ignorez ? matDefaultIgnoreZ : matDefault;
				case chams_type_t::flat:
					return ignorez ? matFlat : matFlatIgnoreZ;
				default:
					return ignorez ? matDefaultIgnoreZ : matDefault;
				}
			}

		private:
			IMaterial* matDefault = nullptr;
			IMaterial* matDefaultIgnoreZ = nullptr;

			IMaterial* matFlat = nullptr;
			IMaterial* matFlatIgnoreZ = nullptr;

			IMaterial* matWireframe = nullptr;
			IMaterial* matWireframeIgnoreZ = nullptr;
		};
	}
}
