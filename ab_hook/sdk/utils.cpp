#include "utils.hpp"

#include "../tools.hpp"
#include "../globals.hpp"
#include "../math.hpp"

int g_screen_w = 0, g_screen_h = 0;

bool world_to_screen( const vector3& in, vector3& out )
{
	static auto& w2sMatrix = g_engine_client->WorldToScreenMatrix( );

	const float w = w2sMatrix.m[3][0] *
		in.x + w2sMatrix.m[3][1] *
		in.y + w2sMatrix.m[3][2] *
		in.z + w2sMatrix.m[3][3];

	if ( w < 0.001f )
		return(false);

	float fl_x = w2sMatrix.m[0][3] +
		w2sMatrix.m[0][0] * in.x +
		w2sMatrix.m[0][1] * in.y +
		w2sMatrix.m[0][2] * in.z;

	float fl_y = w2sMatrix.m[1][3] +
		w2sMatrix.m[1][0] * in.x +
		w2sMatrix.m[1][1] * in.y +
		w2sMatrix.m[1][2] * in.z;

	fl_x /= w;
	fl_y /= w;

	out.x = (g_screen_w / 2.f) + (fl_x * g_screen_w) / 2.f;
	out.y = (g_screen_h / 2.f) - (fl_y * g_screen_h) / 2.f;

	return(true);
}

bool world_to_screen( const vector3& in, int& x, int& y )
{
	static auto& w2sMatrix = g_engine_client->WorldToScreenMatrix( );

	const float w = w2sMatrix.m[3][0] *
		in.x + w2sMatrix.m[3][1] *
		in.y + w2sMatrix.m[3][2] *
		in.z + w2sMatrix.m[3][3];

	if ( w < 0.001f )
		return(false);

	float fl_x = w2sMatrix.m[0][3] +
		w2sMatrix.m[0][0] * in.x +
		w2sMatrix.m[0][1] * in.y +
		w2sMatrix.m[0][2] * in.z;

	float fl_y = w2sMatrix.m[1][3] +
		w2sMatrix.m[1][0] * in.x +
		w2sMatrix.m[1][1] * in.y +
		w2sMatrix.m[1][2] * in.z;

	fl_x /= w;
	fl_y /= w;

	x = static_cast< int >((g_screen_w / 2.f) + (fl_x * g_screen_w) / 2.f);
	y = static_cast< int >((g_screen_h / 2.f) - (fl_y * g_screen_h) / 2.f);

	return(true);
}

void normalize_angles( vector3& angles )
{
	if ( angles.x > 89.0f )
		angles.x = 89.0f;

	if ( angles.x < -89.0f )
		angles.x = -89.0f;

	while ( angles.y > 180.f )
		angles.y -= 360.f;

	while ( angles.y < -180.f )
		angles.y += 360.f;

	angles.z = 0.f;
}

void( __cdecl* con_msg )(const char* format, ...) = [] ( )
{
	HMODULE tier0 = 0;

	while ( !(tier0 = GetModuleHandle( "tier0.dll" )) )
		Sleep( 50 );

	using con_msg_t = decltype(con_msg);

	con_msg_t con_msg_fn = nullptr;

	while ( !(con_msg_fn = ( con_msg_t )GetProcAddress( tier0, "?ConMsg@@YAXPBDZZ" )) )
		Sleep( 50 );

	return(con_msg_fn);
}();
