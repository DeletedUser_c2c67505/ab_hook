#pragma once

#include <stdint.h>
#include <windows.h>

#include "../math_classes/vector3.hpp"
#include "../math_classes/vector2.hpp"
#include "../math_classes/vmatrix.hpp"
#include "../math_classes/qangle.hpp"
#include "../math_classes/matrix3x4.hpp"

/* structs including */
#include "color.hpp"
#include "user_cmd.hpp"

/* enums, defines, constants including */
#include "player_flags.hpp"
#include "in_buttons.hpp"
#include "convar_flags.hpp"

/* used by every interface */
#include "interfaces/virtual.hpp"

/* interfaces including */
#include "interfaces/IPanel.hpp"
#include "interfaces/IEntityList.hpp"
#include "interfaces/IEngineClient.hpp"
#include "interfaces/INetChannel.hpp"
#include "interfaces/IAppSystem.hpp"
#include "interfaces/ISurface.hpp"
#include "interfaces/ConVar.hpp"
#include "interfaces/ICvar.hpp"
#include "interfaces/IMaterial.hpp"
#include "interfaces/IVModelInfo.hpp"
#include "interfaces/IClientRenderable.hpp"
#include "interfaces/IVModelRender.hpp"
#include "interfaces/IMaterialSystem.hpp"
#include "interfaces/IVRenderView.hpp"
#include "interfaces/IPlayerInfoManager.hpp"
#include "interfaces/IGameEventManager2.hpp"
