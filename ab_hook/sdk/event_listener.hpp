#pragma once

#include "../globals.hpp"

class event_listener : public IGameEventListener2
{
public:
	using event_callback_t = void(*)(IGameEvent*);

	event_listener(const char* name, event_callback_t callback, bool server_side = false)
		: m_callback(callback)
	{
		g_game_event_manager->AddListener(this, name, server_side);
	}
	~event_listener()
	{
		g_game_event_manager->RemoveListener(this);
	}
	void FireGameEvent(IGameEvent* event) override
	{
		m_callback(event);
	}
	int GetEventDebugID() override
	{
		/* magic number */
		return 0x2A;
	}
private:
	event_callback_t m_callback = nullptr;
};