#pragma once

#include "../sdk.hpp"

class CNetChan;

#define MAX_PLAYER_NAME_LENGTH	32
#define SIGNED_GUID_LEN			32
#define MAX_CUSTOM_FILES		4

typedef unsigned int CRC32_t;

struct player_info_t
{
	// scoreboard information
	char			name[MAX_PLAYER_NAME_LENGTH];
	// local server user ID, unique while server is running
	int				userID;
	// global unique player identifer
	char			guid[SIGNED_GUID_LEN + 1];
	// friends identification number
	uint32_t		friendsID;
	// friends name
	char			friendsName[MAX_PLAYER_NAME_LENGTH];
	// true, if player is a bot controlled by game.dll
	bool			fakeplayer;
	// true if player is the HLTV proxy
	bool			ishltv;
	// custom files CRC for this player
	CRC32_t			customFiles[MAX_CUSTOM_FILES];
	// this counter increases each time the server downloaded a new file
	unsigned char	filesDownloaded;
};

class IEngineClient
{
public:
	void GetScreenSize(int& width, int& height)
	{
		def_virtual(void)(IEngineClient*, int&, int&);
		use_virtual(5)(this, width, height);
	}
	void ServerCmd(const char* chCommandString, bool bReliable = true)
	{
		def_virtual(void)(IEngineClient*, const char*, bool);
		use_virtual(6)(this, chCommandString, bReliable);
	}
	bool GetPlayerInfo(int ent_num, player_info_t* info)
	{
		def_virtual(bool)(IEngineClient*, int, player_info_t*);
		use_virtual(8)(this, ent_num, info);
	}
	int GetPlayerForUserID(int userID)
	{
		def_virtual(bool)(IEngineClient*, int);
		use_virtual(9)(this, userID);
	}
	bool Con_IsVisible()
	{
		def_virtual(bool)(IEngineClient*);
		use_virtual(11)(this);
	}
	int GetLocalPlayer()
	{
		def_virtual(int)(IEngineClient*);
		use_virtual(12)(this);
	}
	void GetViewAngles(vector3& angles)
	{
		def_virtual(void)(IEngineClient*, vector3&);
		use_virtual(19)(this, angles);
	}
	void SetViewAngles(vector3& angles)
	{
		def_virtual(void)(IEngineClient*, vector3&);
		use_virtual(20)(this, angles);
	}
	int GetMaxClients()
	{
		def_virtual(int)(IEngineClient*);
		use_virtual(21)(this);
	}
	bool IsInGame()
	{
		def_virtual(bool)(IEngineClient*);
		use_virtual(26)(this);
	}
	bool IsConnected()
	{
		def_virtual(bool)(IEngineClient*);
		use_virtual(27)(this);
	}
	bool IsDrawingLoadingImage()
	{
		def_virtual(bool)(IEngineClient*);
		use_virtual(28)(this);
	}
	vmatrix_t& WorldToScreenMatrix()
	{
		def_virtual(vmatrix_t&)(IEngineClient*);
		use_virtual(36)(this);
	}
	bool IsTakingScreenshot()
	{
		def_virtual(bool)(IEngineClient*);
		use_virtual(85)(this);
	}
	CNetChan* GetNetChannelInfo()
	{
		def_virtual(CNetChan*)(IEngineClient*);
		use_virtual(72)(this);
	}
	void ClientCmd_Unrestricted(const char* chCommandString)
	{
		def_virtual(void)(IEngineClient*, const char*);
		use_virtual(106)(this, chCommandString);
	}
	int GetAppId()
	{
		def_virtual(int)(IEngineClient*);
		use_virtual(104)(this);
	}
};