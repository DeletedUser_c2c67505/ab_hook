#pragma once

/* generates typedef for virtual function */
#define def_virtual(ret) using original_fn = ret(__thiscall*)
/* gives virtual method via index */
#define get_virtual(type, object, index) ((*(type**)object)[index])
/* calls virtual method by index and returns its result */
#define use_virtual(index) return get_virtual(original_fn, this, index)

#define trace_unknown_virtual1(x) virtual void __unknown_##x () = 0
#define trace_unknown_virtual2(x) trace_unknown_virtual1(x)
/* makes dummy method */
#define unknown_virtual trace_unknown_virtual2(__COUNTER__)
