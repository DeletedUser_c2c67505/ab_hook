#pragma once

#include "../sdk.hpp"

class CPhysCollide;
class CUtlBuffer;
class CGameTrace;
class IModelLoadCallback;
class IClientRenderable;

struct virtualmodel_t;

typedef CGameTrace trace_t;
typedef unsigned short MDLHandle_t;

struct model_t;


struct vcollide_t
{
	unsigned short	solidCount : 15;
	unsigned short	isPacked : 1;
	unsigned short	descSize;
	// VPhysicsSolids
	CPhysCollide**	solids;
	char*			pKeyValues;
};

struct cplane_t
{
	vector3	normal;
	float	dist;
	byte	type;			// for fast side tests
	byte	signbits;		// signx + (signy<<1) + (signz<<1)
	byte	pad[2];

	cplane_t() { }

private:
	// No copy constructors allowed if we're in optimal mode
	cplane_t(const cplane_t& vOther);
};

struct mstudiobbox_t
{
	int32_t m_Bone;
	int32_t m_Group; // intersection group
	vector3 m_vBbmin; // bounding box 
	vector3 m_vBbmax;
	int32_t m_Szhitboxnameindex; // offset to the name of the hitbox.
	int32_t pad[3];
	float m_flRadius;
	int32_t pad2[4];

	char* GetHitboxName(void)
	{
		if (m_Szhitboxnameindex == 0)
			return "";

		return ((char*)this) + m_Szhitboxnameindex;
	}
};

struct mstudiohitboxset_t
{
	int						sznameindex;
	inline char* const		GetName(void) const { return ((char*)this) + sznameindex; }
	int						numhitboxes;
	int						hitboxindex;
	inline mstudiobbox_t*	pHitbox(int i) const { return (mstudiobbox_t*)(((BYTE*)this) + hitboxindex) + i; };
};

struct mstudiobone_t
{
	int					sznameindex;
	inline char * const GetName(void) const { return ((char *)this) + sznameindex; }
	int		 			parent;
	int					bonecontroller[6];

	vector3				pos;
	float				quat[4];
	vector3				rot;
	vector3				posscale;
	vector3				rotscale;

	matrix3x4_t			poseToBone;
	float				qAlignment[4];
	int					flags;
	int					proctype;
	int					procindex;		// procedural rule
	mutable int			physicsbone;	// index into physically simulated bone
	inline void *		GetProcedure() const { if (procindex == 0) return NULL; else return  (void *)(((BYTE *)this) + procindex); };
	int					surfacepropidx;	// index into string tablefor property name
	inline char * const GetSurfaceProps(void) const { return ((char *)this) + surfacepropidx; }
	int					contents;		// See BSPFlags.h for the contents flags

	int					unused[8];		// remove as appropriate
};

struct studiohdr_t
{
	int					id;
	int					version;
	int					checksum;
	char				name[64];
	int					length;
	vector3				eyeposition;
	vector3				illumposition;
	vector3				hull_min;
	vector3				hull_max;
	vector3				view_bbmin;
	vector3				view_bbmax;
	int					flags;
	int					numbones;
	int					boneindex;

	inline mstudiobone_t *pBone(int i) const { return (mstudiobone_t *)(((BYTE *)this) + boneindex) + i; };

	int					numbonecontrollers;
	int					bonecontrollerindex;
	int					numhitboxsets;
	int					hitboxsetindex;

	mstudiohitboxset_t* pHitboxSet(int i) const
	{
		return (mstudiohitboxset_t*)(((BYTE*)this) + hitboxsetindex) + i;
	}

	inline mstudiobbox_t* pHitbox(int i, int set) const
	{
		mstudiohitboxset_t const* s = pHitboxSet(set);

		if (!s)
			return NULL;

		return s->pHitbox(i);
	}

	inline int GetHitboxCount(int set) const
	{
		mstudiohitboxset_t const* s = pHitboxSet(set);

		if (!s)
			return 0;

		return s->numhitboxes;
	}

	int				numlocalanim;
	int				localanimindex;
	int				numlocalseq;
	int				localseqindex;
	mutable int		activitylistversion;
	mutable int		eventsindexed;
	int				numtextures;
	int				textureindex;
	int				numcdtextures;
	int				cdtextureindex;
	int				numskinref;
	int				numskinfamilies;
	int				skinindex;
	int				numbodyparts;
	int				bodypartindex;
	int				numlocalattachments;
	int				localattachmentindex;
	int				numlocalnodes;
	int				localnodeindex;
	int				localnodenameindex;
	int				numflexdesc;
	int				flexdescindex;
	int				numflexcontrollers;
	int				flexcontrollerindex;
	int				numflexrules;
	int				flexruleindex;
	int				numikchains;
	int				ikchainindex;
	int				nummouths;
	int				mouthindex;
	int				numlocalposeparameters;
	int				localposeparamindex;
	int				surfacepropindex;
	int				keyvalueindex;
	int				keyvaluesize;
	int				numlocalikautoplaylocks;
	int				localikautoplaylockindex;
	float			mass;
	int				contents;
	int				numincludemodels;
	int				includemodelindex;
	mutable void	*virtualModel;
	int				szanimblocknameindex;
	int				numanimblocks;
	int				animblockindex;
	mutable void	*animblockModel;
	int				bonetablebynameindex;
	void			*pVertexBase;
	void			*pIndexBase;
	uint8_t			constdirectionallightdot;
	uint8_t			rootLOD;
	uint8_t			numAllowedRootLODs;
	uint8_t pad1[0x5];
	int				numflexcontrollerui;
	int				flexcontrolleruiindex;
	float			flVertAnimFixedPointScale;
	uint8_t pad2[0x4];
	int				studiohdr2index;
	uint8_t pad3[0x4];
};

class IVModelInfo
{
public:
	virtual							~IVModelInfo() { }

	// Returns model_t* pointer for a model given a precached or dynamic model index.
	virtual const model_t*			GetModel(int modelindex) = 0;

	// Returns index of model by name for precached or known dynamic models.
	// Does not adjust reference count for dynamic models.
	virtual int						GetModelIndex(const char* name) const = 0;

	// Returns name of model
	virtual const char*				GetModelName(const model_t* model) const = 0;
	virtual vcollide_t*				GetVCollide(const model_t* model) = 0;
	virtual vcollide_t*				GetVCollide(int modelindex) = 0;
	virtual void					GetModelBounds(const model_t* model, vector3& mins, vector3& maxs) const = 0;
	virtual	void					GetModelRenderBounds(const model_t* model, vector3& mins, vector3& maxs) const = 0;
	virtual int						GetModelFrameCount(const model_t* model) const = 0;
	virtual int						GetModelType(const model_t* model) const = 0;
	virtual void*					GetModelExtraData(const model_t* model) = 0;
	virtual bool					ModelHasMaterialProxy(const model_t* model) const = 0;
	virtual bool					IsTranslucent(model_t const* model) const = 0;
	virtual bool					IsTranslucentTwoPass(const model_t* model) const = 0;
	virtual void					RecomputeTranslucency(const model_t* model, int nSkin, int nBody, IClientRenderable* pClientRenderable, float fInstanceAlphaModulate = 1.0f) = 0;
	virtual int						GetModelMaterialCount(const model_t* model) const = 0;
	virtual void					GetModelMaterials(const model_t* model, int count, IMaterial** ppMaterial) = 0;
	virtual bool					IsModelVertexLit(const model_t* model) const = 0;
	virtual const char*				GetModelKeyValueText(const model_t* model) = 0;
	virtual bool					GetModelKeyValue(const model_t* model, CUtlBuffer &buf) = 0; // supports keyvalue blocks in submodels
	virtual float					GetModelRadius(const model_t* model) = 0;

	virtual const studiohdr_t*		FindModel(const studiohdr_t* pStudioHdr, void** cache, const char* modelname) const = 0;
	virtual const studiohdr_t*		FindModel(void* cache) const = 0;
	virtual	virtualmodel_t*			GetVirtualModel(const studiohdr_t* pStudioHdr) const = 0;
	virtual byte*					GetAnimBlock(const studiohdr_t* pStudioHdr, int iBlock) const = 0;

	// Available on client only!!!
	virtual void					GetModelMaterialColorAndLighting(const model_t* model, vector3 const& origin,
		vector3 const& angles, trace_t* pTrace, vector3& lighting, vector3& matColor) = 0;
	virtual void					GetIlluminationPoint(const model_t* model, IClientRenderable* pRenderable, vector3 const& origin,
		vector3 const& angles, vector3* pLightingCenter) = 0;

	virtual int						GetModelContents(int modelIndex) = 0;
	virtual studiohdr_t*			GetStudioModel(const model_t* mod) = 0;
	virtual int						GetModelSpriteWidth(const model_t* model) const = 0;
	virtual int						GetModelSpriteHeight(const model_t* model) const = 0;

	// Sets/gets a map-specified fade range (client only)
	virtual void					SetLevelScreenFadeRange(float flMinSize, float flMaxSize) = 0;
	virtual void					GetLevelScreenFadeRange(float* pMinArea, float* pMaxArea) const = 0;

	// Sets/gets a map-specified per-view fade range (client only)
	virtual void					SetViewScreenFadeRange(float flMinSize, float flMaxSize) = 0;

	// Computes fade alpha based on distance fade + screen fade (client only)
	virtual unsigned char			ComputeLevelScreenFade(const vector3 &vecAbsOrigin, float flRadius, float flFadeScale) const = 0;
	virtual unsigned char			ComputeViewScreenFade(const vector3 &vecAbsOrigin, float flRadius, float flFadeScale) const = 0;

	// both client and server
	virtual int						GetAutoplayList(const studiohdr_t* pStudioHdr, unsigned short** pAutoplayList) const = 0;

	// Gets a virtual terrain collision model (creates if necessary)
	// NOTE: This may return NULL if the terrain model cannot be virtualized
	virtual CPhysCollide*			GetCollideForVirtualTerrain(int index) = 0;

	virtual bool					IsUsingFBTexture(const model_t* model, int nSkin, int nBody, void /*IClientRenderable*/* pClientRenderable) const = 0;

	// Obsolete methods. These are left in to maintain binary compatibility with clients using the IVModelInfo old version.
	virtual const model_t*			FindOrLoadModel(const char* name) {}
	virtual void					InitDynamicModels() { }
	virtual void					ShutdownDynamicModels() {  }
	virtual void					AddDynamicModel(const char* name, int nModelIndex = -1) {  }
	virtual void					ReferenceModel(int modelindex) { }
	virtual void					UnreferenceModel(int modelindex) {  }
	virtual void					CleanupDynamicModels(bool bForce = false) {  }

	virtual MDLHandle_t				GetCacheHandle(const model_t* model) const = 0;

	// Returns planes of non-nodraw brush model surfaces
	virtual int						GetBrushModelPlaneCount(const model_t* model) const = 0;
	virtual void					GetBrushModelPlane(const model_t* model, int nIndex, cplane_t &plane, vector3* pOrigin) const = 0;
	virtual int						GetSurfacepropsForVirtualTerrain(int index) = 0;

	// Poked by engine host system
	virtual void					OnLevelChange() = 0;

	virtual int						GetModelClientSideIndex(const char* name) const = 0;

	// Returns index of model by name, dynamically registered if not already known.
	virtual int						RegisterDynamicModel(const char* name, bool bClientSide) = 0;

	virtual bool					IsDynamicModelLoading(int modelIndex) = 0;

	virtual void					AddRefDynamicModel(int modelIndex) = 0;
	virtual void					ReleaseDynamicModel(int modelIndex) = 0;

	// Registers callback for when dynamic model has finished loading.
	// Automatically adds reference, pair with ReleaseDynamicModel.
	virtual bool					RegisterModelLoadCallback(int modelindex, IModelLoadCallback* pCallback, bool bCallImmediatelyIfLoaded = true) = 0;
	virtual void					UnregisterModelLoadCallback(int modelindex, IModelLoadCallback* pCallback) = 0;
};
