#pragma once

#include "../sdk.hpp"

class IConCommandBaseAccessor;

class IConVar
{
public:
	virtual void SetValue(const char *pValue) = 0;
	virtual void SetValue(float flValue) = 0;
	virtual void SetValue(int nValue) = 0;

	virtual const char* GetName(void) const = 0;

	virtual bool IsFlagSet(int nFlag) const = 0;
};

class ConCommandBase
{
public:
	unknown_virtual;									//Constructor
	unknown_virtual;									//Constructor

	unknown_virtual;									//Destructor

	unknown_virtual;									//IsCommand
	unknown_virtual;									//IsFlagSet
	unknown_virtual;									//AddFlags
	unknown_virtual;									//GetName
	unknown_virtual;									//GetHelpText
	unknown_virtual;									//IsRegistered
	unknown_virtual;									//GetDLLIdentifier

protected:
	unknown_virtual;									//CreateBase
	unknown_virtual;									//Init

public:
	ConCommandBase*					m_pNext;
	bool							m_bRegistered;
	const char* 					m_pszName;
	const char* 					m_pszHelpString;
	int								m_nFlags;

protected:
	static ConCommandBase*			s_pConCommandBases;
	static IConCommandBaseAccessor*	s_pAccessor;
};


class ConVar : public ConCommandBase, public IConVar
{
public:
	char* GetName()
	{
		def_virtual(char*)(ConVar*);
		use_virtual(9)(this);
	}
	void SetValue(const char* value)
	{
		def_virtual(void)(ConVar*, const char*);
		use_virtual(12)(this, value);
	}
	void SetValue(float value)
	{
		def_virtual(void)(ConVar*, float);
		use_virtual(13)(this, value);
	}
	void SetValue(int value)
	{
		def_virtual(void)(ConVar*, int);
		use_virtual(14)(this, value);
	}

	const char*		GetDefault();
	int				GetInt();
	float			GetFloat();
	bool			GetBool();

	ConVar*			m_pParent;
	const char*		m_pszDefaultValue;
	char*			m_pszString;
	int				m_StringLength;
	float			m_fValue;
	int				m_nValue;
	bool			m_bHasMin;
	float			m_fMinVal;
	bool			m_bHasMax;
	float			m_fMaxVal;
	void*			m_fnChangeCallback;
};

class SpoofedConvar
{
public:
	SpoofedConvar();
	SpoofedConvar(const char* szCVar);
	SpoofedConvar(ConVar* pCVar);

	~SpoofedConvar();
	bool	IsSpoofed();
	void	Spoof();

	void	SetFlags(int flags);
	int		GetFlags();

	void	SetBool(bool bValue);
	void	SetInt(int iValue);
	void	SetFloat(float flValue);
	void	SetString(const char* szValue);

public:
	ConVar*	m_pOriginalCVar = nullptr;
	ConVar*	m_pDummyCVar = nullptr;

	char	m_szDummyName[128];
	char	m_szDummyValue[128];
	char	m_szOriginalName[128];
	char	m_szOriginalValue[128];
	int		m_iOriginalFlags;
};
