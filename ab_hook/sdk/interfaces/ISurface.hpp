#pragma once

#include "../sdk.hpp"


enum ETextureFormat
{
	eTextureFormat_RGBA,
	eTextureFormat_BGRA,
	eTextureFormat_BGRA_Opaque
};

enum SurfaceFeature_e
{
	ANTIALIASED_FONTS = 1,
	DROPSHADOW_FONTS,
	ESCAPE_KEY,
	OPENING_NEW_HTML_WINDOWS,
	FRAME_MINIMIZE_MAXIMIZE,
	OUTLINE_FONTS,
	DIRECT_HWND_RENDER,
};

enum EFontFlags	
{
	FONTFLAG_NONE,
	FONTFLAG_ITALIC			= 0x001,
	FONTFLAG_UNDERLINE		= 0x002,
	FONTFLAG_STRIKEOUT		= 0x004,
	FONTFLAG_SYMBOL			= 0x008,
	FONTFLAG_ANTIALIAS		= 0x010,
	FONTFLAG_GAUSSIANBLUR	= 0x020,
	FONTFLAG_ROTARY			= 0x040,
	FONTFLAG_DROPSHADOW		= 0x080,
	FONTFLAG_ADDITIVE		= 0x100,
	FONTFLAG_OUTLINE		= 0x200,
	FONTFLAG_CUSTOM			= 0x400,		// custom generated font - never fall back to asian compatibility mode
	FONTFLAG_BITMAP			= 0x800,		// compiled bitmap font - no fallbacks
};

enum FontDrawType_t
{
	FONT_DRAW_DEFAULT = 0,
	FONT_DRAW_NONADDITIVE,
	FONT_DRAW_ADDITIVE,
	FONT_DRAW_TYPE_COUNT = 2,
};

struct vertex_t;

class ISurface
{
public:
	void DrawSetColor(int r, int g, int b, int a)
	{
		def_virtual(void)(ISurface*, int, int, int, int);
		use_virtual(11)(this, r, g, b, a);
	}
	void DrawFilledRect(int x0, int y0, int x1, int y1)
	{
		def_virtual(void)(ISurface*, int, int, int, int);
		use_virtual(12)(this, x0, y0, x1, y1);
	}
	void DrawOutlinedRect(int x0, int y0, int x1, int y1)
	{
		def_virtual(void)(ISurface*, int, int, int, int);
		use_virtual(14)(this, x0, y0, x1, y1);
	}
	void DrawLine(int x0, int y0, int x1, int y1)
	{
		def_virtual(void)(ISurface*, int, int, int, int);
		use_virtual(15)(this, x0, y0, x1, y1);
	}
	void DrawSetTextFont(unsigned font)
	{
		def_virtual(void)(ISurface*, unsigned);
		use_virtual(17)(this, font);
	}
	void DrawSetTextColor(int r, int g, int b, int a)
	{
		def_virtual(void)(ISurface*, int, int, int, int);
		use_virtual(19)(this, r, g, b, a);
	}
	void DrawSetTextColor(color_t color)
	{
		DrawSetTextColor(color.r, color.g, color.b, color.a);
	}
	void DrawSetTextPos(int x, int y)
	{
		def_virtual(void)(ISurface*, int, int);
		use_virtual(20)(this, x, y);
	}
	void DrawPrintText(const wchar_t* text, int len, FontDrawType_t drawType = FONT_DRAW_DEFAULT)
	{
		def_virtual(void)(ISurface*, const wchar_t*, int, FontDrawType_t);
		use_virtual(22)(this, text, len, drawType);
	}
	void DrawSetTextureRGBA(int x, unsigned char const* woah, int eh, int ah, int bh = 0, bool neat = true)
	{
		def_virtual(void)(ISurface*, int, unsigned char const*, int, int, int, bool);
		use_virtual(31)(this, x, woah, eh, ah, bh, neat);
	}
	void DrawSetTexture(int x)
	{
		def_virtual(void)(ISurface*, int);
		use_virtual(32)(this, x);
	}
	void DrawTexturedRect(int x, int y, int width, int height)
	{
		def_virtual(void)(ISurface*, int, int, int, int);
		use_virtual(34)(this, x, y, width, height);
	}
#ifdef CreateFont
#undef CreateFont
#endif
	unsigned CreateFont()
	{
		def_virtual(unsigned)(ISurface*);
		use_virtual(66)(this);
	}
	void SetFontGlyphSet(unsigned& font, const char* font_name, int tall, int weight, int blur, int scanlines, int flags)
	{
		def_virtual(void)(ISurface*, unsigned long, const char*, int, int, int, int, int, int, int);
		use_virtual(67)(this, font, font_name, tall, weight, blur, scanlines, flags, 0, 0);
	}
	void GetTextSize(unsigned long font, const wchar_t* text, int& wide, int& tall)
	{
		def_virtual(void)(ISurface*, unsigned long, const wchar_t*, int&, int&);
		use_virtual(75)(this, font, text, wide, tall);
	}
	void GetCursorPosition(int& x, int& y)
	{
		def_virtual(void)(ISurface*, int&, int&);
		use_virtual(96)(this, x, y);
	}
	int CreateNewTextureID(bool b = true)
	{
		def_virtual(int)(ISurface*, bool);
		use_virtual(37)(this, b);
	}
	void DrawTexturedPolygon(int n, vertex_t* vertices, bool clip_verticies = true)
	{
		def_virtual(void)(ISurface*, int, vertex_t*, bool);
		use_virtual(102)(this, n, vertices, clip_verticies);
	}
	void PlaySoundB(const char* sound)
	{
		def_virtual(void)(ISurface*, const char*);
		use_virtual(78)(this, sound);
	}
};