#pragma once

#include "../sdk.hpp"


struct ColorMeshInfo_t;
struct studiohwdata_t;
struct StudioDecalHandle_t;
struct DrawModelInfo_t;
struct MaterialLightingState_t;
struct Ray_t;
struct DataCacheHandle_t;
struct LightCacheHandle_t;
struct studiohdr_t;
struct model_t;

class IMatRenderContext;
class ITexture;
class CStudioHdr;


typedef unsigned short ModelInstanceHandle_t;

struct DrawModelState_t
{
	studiohdr_t*				m_pStudioHdr;
	studiohwdata_t*				m_pStudioHWData;
	IClientRenderable*			m_pRenderable;
	const float*				m_pModelToWorld;
	StudioDecalHandle_t*		m_decals;
	int							m_drawFlags;
	int							m_lod;
};

struct model_render_info_t
{

	vector3						origin;
	vector3						angles;
	IClientRenderable*			pRenderable;
	const model_t*				pModel;
	const float**				pModelToWorld;
	const float**				pLightingOffset;
	const vector3*				pLightingOrigin;
	int							flags;
	int							entity_index;
	int							skin;
	int							body;
	int							hitboxset;
	ModelInstanceHandle_t		instance;

};

struct draw_model_state_t
{
	studiohdr_t*				m_pStudioHdr;
	studiohwdata_t*				m_pStudioHWData;
	IClientRenderable*			m_pRenderable;
	const float*				m_pModelToWorld;
	StudioDecalHandle_t*		m_decals;
	int							m_drawFlags;
	int							m_lod;
};

enum
{
	MODEL_INSTANCE_INVALID = (ModelInstanceHandle_t)~0
};


struct ModelRenderInfo_t
{
	vector3 origin;
	vector3 angles;
	IClientRenderable *pRenderable;
	const model_t *pModel;
	const float *pModelToWorld;
	const float *pLightingOffset;
	const vector3 *pLightingOrigin;
	int flags;
	int entity_index;
	int skin;
	int body;
	int hitboxset;
	ModelInstanceHandle_t instance;
};

struct StaticPropRenderInfo_t
{
	const float*				pModelToWorld;
	const model_t*				pModel;
	IClientRenderable*			pRenderable;
	vector3*					pLightingOrigin;
	ModelInstanceHandle_t		instance;
	unsigned char				skin;
	unsigned char				alpha;
};

struct LightingQuery_t
{
	vector3						m_LightingOrigin;
	ModelInstanceHandle_t		m_InstanceHandle;
	bool						m_bAmbientBoost;
};

struct StaticLightingQuery_t : public LightingQuery_t
{
	IClientRenderable* m_pRenderable;
};

enum OverrideType_t
{
	OVERRIDE_NORMAL = 0,
	OVERRIDE_BUILD_SHADOWS,
	OVERRIDE_DEPTH_WRITE
};

enum
{
	ADDDECAL_TO_ALL_LODS = -1
};


class IVModelRender
{
public:
	void forced_material_override(IMaterial* newMaterial, OverrideType_t nOverrideType = OVERRIDE_NORMAL)
	{
		def_virtual(void)(IVModelRender*, IMaterial*, OverrideType_t);
		use_virtual(1)(this, newMaterial, nOverrideType);
	}
};
