#include "ConVar.hpp"

#include <stdio.h>

#include "../../globals.hpp"

const char* ConVar::GetDefault()
{
	return m_pszDefaultValue;
}

int ConVar::GetInt()
{
	return m_nValue;
}
float ConVar::GetFloat()
{
	return m_fValue;
}
bool ConVar::GetBool()
{
	return m_nValue != 0;
}

/* SpoofedConVar */
SpoofedConvar::SpoofedConvar() { }

SpoofedConvar::SpoofedConvar(const char* szCVar)
	: m_pOriginalCVar(g_cvar->FindVar(szCVar))
{
	Spoof();
}

SpoofedConvar::SpoofedConvar(ConVar* pCVar)
	: m_pOriginalCVar(pCVar)
{
	Spoof();
}

SpoofedConvar::~SpoofedConvar()
{
	if (!IsSpoofed())
		return;

	DWORD dwOld;

	SetFlags(m_iOriginalFlags);
	SetString(m_szOriginalValue);

	VirtualProtect((LPVOID)m_pOriginalCVar->m_pszName, 128, PAGE_READWRITE, &dwOld);
	strcpy((char*)m_pOriginalCVar->m_pszName, m_szOriginalName);
	VirtualProtect((LPVOID)m_pOriginalCVar->m_pszName, 128, dwOld, &dwOld);

	//Unregister dummy cvar
	g_cvar->UnregisterConCommand(reinterpret_cast<ConCommandBase*>(m_pDummyCVar));
	free(m_pDummyCVar);
	m_pDummyCVar = NULL;
}

bool SpoofedConvar::IsSpoofed()
{
	return m_pDummyCVar != NULL;
}

void SpoofedConvar::Spoof()
{
	if (IsSpoofed() || !m_pOriginalCVar)
		return;

	//Save old name value and flags so we can restore the cvar lates if needed
	m_iOriginalFlags = m_pOriginalCVar->m_nFlags;
	strcpy(m_szOriginalName, m_pOriginalCVar->m_pszName);
	strcpy(m_szOriginalValue, m_pOriginalCVar->m_pszDefaultValue);

	sprintf_s(m_szDummyName, "d_%s", m_szOriginalName);

	//Create the dummy cvar
	m_pDummyCVar = (ConVar*)malloc(sizeof(ConVar));
	if (!m_pDummyCVar)
		return;

	memcpy(m_pDummyCVar, m_pOriginalCVar, sizeof(ConVar));

	m_pDummyCVar->m_pNext = NULL;
	//Register it
	g_cvar->RegisterConCommand(reinterpret_cast<ConCommandBase*>(m_pDummyCVar));

	//Fix "write access violation" bullshit
	DWORD dwOld;
	VirtualProtect((LPVOID)m_pOriginalCVar->m_pszName, 128, PAGE_READWRITE, &dwOld);

	//Rename the cvar
	strcpy((char*)m_pOriginalCVar->m_pszName, m_szDummyName);

	VirtualProtect((LPVOID)m_pOriginalCVar->m_pszName, 128, dwOld, &dwOld);

	SetFlags(FCVAR_NONE);
}

void SpoofedConvar::SetFlags(int flags)
{
	if (IsSpoofed())
		m_pOriginalCVar->m_nFlags = flags;
}

int SpoofedConvar::GetFlags()
{
	return m_pOriginalCVar->m_nFlags;
}

void SpoofedConvar::SetBool(bool bValue)
{
	if (IsSpoofed())
		m_pOriginalCVar->SetValue(bValue);
}

void SpoofedConvar::SetInt(int iValue)
{
	if (IsSpoofed())
		m_pOriginalCVar->SetValue(iValue);
}

void SpoofedConvar::SetFloat(float flValue)
{
	if (IsSpoofed())
		m_pOriginalCVar->SetValue(flValue);
}

void SpoofedConvar::SetString(const char* szValue)
{
	if (IsSpoofed())
		m_pOriginalCVar->SetValue(szValue);
}