#pragma once

struct	edict_t;

class	IPlayerInfo;
class	CSaveRestoreData;

typedef int string_t;

class CGlobalVarsBase
{
public:
	CGlobalVarsBase(bool bIsClient) :
		m_bClient(bIsClient),
		nTimestampNetworkingBase(100),
		nTimestampRandomizeWindow(32) { }

	bool IsClient() const
	{
		return m_bClient;
	}

	int GetNetworkBase(int nTick, int nEntity)
	{
		int nEntityMod = nEntity % nTimestampRandomizeWindow;
		int nBaseTick = nTimestampNetworkingBase * (int)((nTick - nEntityMod) / nTimestampNetworkingBase);
		return nBaseTick;
	}

public:
	float				realtime;
	int					framecount;
	float				absoluteframetime;
	float				curtime;
	float				frametime;
	int					maxClients;
	int					tickcount;
	float				interval_per_tick;
	float				interpolation_amount;
	int					simTicksThisFrame;
	int					network_protocol;
	CSaveRestoreData*	pSaveData;

private:
	bool				m_bClient;
	int					nTimestampNetworkingBase;
	int					nTimestampRandomizeWindow;
};

enum MapLoadType_t
{
	MapLoad_NewGame = 0,
	MapLoad_LoadGame,
	MapLoad_Transition,
	MapLoad_Background,
};

class CGlobalVars : public CGlobalVarsBase
{
public:
	CGlobalVars(bool bIsClient) : 
		CGlobalVarsBase(bIsClient),
		serverCount(0) { }

public:

	string_t		mapname;
	int				mapversion;
	string_t		startspot;
	MapLoadType_t	eLoadType;		
	bool			bMapLoadFailed;	

	bool			deathmatch;
	bool			coop;
	bool			teamplay;

	int				maxEntities;

	int				serverCount;
};

class IPlayerInfoManager
{
public:
	virtual IPlayerInfo* GetPlayerInfo(edict_t* pEdict) = 0;
	virtual CGlobalVars* GetGlobalVars() = 0;
};