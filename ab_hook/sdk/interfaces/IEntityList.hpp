#pragma once

#include "../sdk.hpp"

class IEntityList
{
public:
	unknown_virtual;
	unknown_virtual;
	unknown_virtual;
	virtual unsigned GetClientEntity(int index) = 0;
};
