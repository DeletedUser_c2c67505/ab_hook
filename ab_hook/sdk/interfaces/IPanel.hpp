#pragma once

#include "../sdk.hpp"

using VPANEL = unsigned int;

class IPanel
{
public:
	const char* GetName(VPANEL panel)
	{
		def_virtual(const char*)(IPanel*, VPANEL);
		use_virtual(36)(this, panel);
	}
};