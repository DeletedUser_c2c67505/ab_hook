#pragma once

#include "../vector3.h"

struct model_t
{
	void* handle;
	char name[260];
	int loadFlags;
	vector3  vecMaxs;
	float   radius;
};