#pragma once

#include "sdk.hpp"

struct user_cmd_t
{
	virtual ~user_cmd_t() { }

	int command_number;			//4
	int tick_count;				//8
	vector3 view_angles;		//C
	float forward_move;			//18
	float side_move;			//1C
	float up_move;				//20
	int	buttons;				//24
	int impulse;				//28
	int weapon_select;			//2C
	int weapon_subtype;			//30
	int random_seed;			//34
	short mousedx;				//38
	short mousedy;				//3A
	bool has_been_predicted;	//3C;
};
