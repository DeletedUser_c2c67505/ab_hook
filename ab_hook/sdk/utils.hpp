#pragma once

#include "../math_classes/vector3.hpp"
#include "../math_classes/vmatrix.hpp"

/* game window width */
extern int g_screen_w;
/* game window height */
extern int g_screen_h;

/* converts world coordinates to screen position, returns true if successful */
bool world_to_screen( const vector3& in, int& x, int& y );
bool world_to_screen( const vector3& in, vector3& out );
/* normalizes angle to valid values */
void normalize_angles( vector3& angle );

extern void( __cdecl* con_msg )(const char* format, ...);
