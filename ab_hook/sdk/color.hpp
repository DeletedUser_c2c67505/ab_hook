#pragma once

struct color_t
{
	int r, g, b, a;

	color_t(
		unsigned char red,
		unsigned char green,
		unsigned char blue,
		unsigned char alpha = 255) :
		r(red),
		g(green),
		b(blue),
		a(alpha)
	{ }
	color_t() :
		r(255),
		g(255),
		b(255),
		a(255)
	{ }

	bool operator==(const color_t& el)
	{
		return(
			this->r == el.r &&
			this->g == el.g &&
			this->b == el.b &&
			this->a == el.a);
	}
};
