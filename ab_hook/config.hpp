#pragma once

#define PROJECT_NAME ab_hook

constexpr struct
{
	int major = 1;
	int minor = 1;
	int patch = 6;
}
g_version;
