#include "math.hpp"

#include <math.h>

namespace math
{
	__forceinline const float square(const float x)
	{
		return(x * x);
	}
	
	const float deg_to_rad(const float deg)
	{
		constexpr float rads_in_deg = pi / 180.f;

		return(deg * rads_in_deg);
	}

	__forceinline const float rad_to_deg(const float rad)
	{
		constexpr float degs_in_rad = 180.f / pi;

		return(rad * degs_in_rad);
	}

	float clamp_deg(float deg)
	{
		constexpr float max_deg = 360.f;

		while (deg >= max_deg)
			deg -= max_deg;

		while (deg < 0.f)
			deg += max_deg;

		return(deg);
	}

	bool screen_transform(const vmatrix_t& w2s_matrix, const vector3& in, float& x, float& y)
	{
		const float w =
			w2s_matrix.m[3][0] * in.x +
			w2s_matrix.m[3][1] * in.y +
			w2s_matrix.m[3][2] * in.z +
			w2s_matrix.m[3][3];

		if (w < 0.001f)
			return(false);

		x = (
			w2s_matrix.m[0][0] * in.x +
			w2s_matrix.m[0][1] * in.y +
			w2s_matrix.m[0][2] * in.z +
			w2s_matrix.m[0][3]) / w;

		y = (
			w2s_matrix.m[1][0] * in.x +
			w2s_matrix.m[1][1] * in.y +
			w2s_matrix.m[1][2] * in.z +
			w2s_matrix.m[1][3]) / w;

		return(true);
	}

	void vector_transform(const vector3& in1, const matrix3x4_t& in2, vector3& out)
	{
		out.x = in1.x * in2[0][0] + in1.y * in2[0][1] + in1.z * in2[0][2] + in2[0][3];
		out.y = in1.x * in2[1][0] + in1.y * in2[1][1] + in1.z * in2[1][2] + in2[1][3];
		out.z = in1.x * in2[2][0] + in1.y * in2[2][1] + in1.z * in2[2][2] + in2[2][3];
	}

	float distance(const vector3& vec1, const vector3& vec2)
	{
		return(sqrtf(
			square(vec1.x - vec2.x) +
			square(vec1.y - vec2.y) +
			square(vec1.z - vec2.z)));
	}

	float get_fov(const vector3& ang1, const vector3& ang2)
	{
		return(sqrtf(
			square(ang1.x - ang2.x) +
			square(ang1.y - ang2.y)));
	}
}