#include "globals.hpp"

#include "hooks.hpp"

#include "logging.hpp"
#include "tools.hpp"
#include "constants.hpp"

#include "features/hitmarker.hpp"
#include "features/menu.hpp"

HWND g_wnd = 0;
HMODULE g_module = 0;

settings_t g_settings = settings_t();

IEntityList* g_entity_list = nullptr;
IEngineClient* g_engine_client = nullptr;
IVModelRender* g_model_render = nullptr;
ICvar* g_cvar = nullptr;
IMaterialSystem* g_material_system = nullptr;
IVModelInfo* g_model_info = nullptr;
IVRenderView* g_render_view = nullptr;
ISurface* g_surface = nullptr;
IPanel* g_panel = nullptr;
IPlayerInfoManager* g_player_info_manager = nullptr;
CGlobalVars* g_global_vars = nullptr;
IGameEventManager2* g_game_event_manager = nullptr;


bool attach(HMODULE current_module)
{
#ifdef _DEBUG
	tools::attach_console();
#endif

	log_info("injected");

	g_wnd = FindWindow(constants::main_window_class_name, NULL);
	log_error(!g_wnd, "g_wnd is null");

	g_module = current_module;
	log_error(!g_module, "g_module is null");
	
	g_entity_list = (IEntityList*)tools::get_interface("client.dll", constants::entity_list_interface);
	log_error(!g_entity_list, "failed to find entity list interface");

	g_engine_client = (IEngineClient*)tools::get_interface("engine.dll", constants::engine_client_interface);
	log_error(!g_engine_client, "failed to find engine client interface");

	g_model_render = (IVModelRender*)tools::get_interface("engine.dll", constants::model_render_interface);
	log_error(!g_model_render, "failed to find model render interface");

	g_cvar = (ICvar*)tools::get_interface("vstdlib.dll", constants::engine_cvar_interface);
	log_error(!g_cvar, "failed to find cvar interface");

	g_material_system = (IMaterialSystem*)tools::get_interface("materialsystem.dll", constants::material_system_interface);
	log_error(!g_material_system, "failed to find material system interface");

	g_render_view = (IVRenderView*)tools::get_interface("engine.dll", constants::render_view_interface);
	log_error(!g_render_view, "failed to find render view interface");

	g_model_info = (IVModelInfo*)tools::get_interface("engine.dll", constants::model_info_interface);
	log_error(!g_model_info, "failed to find model info interface");

	g_surface = (ISurface*)tools::get_interface("vguimatsurface.dll", constants::surface_interface);
	log_error(!g_surface, "failed to find surface interface");

	g_panel = (IPanel*)tools::get_interface("vgui2.dll", constants::panel_interface);
	log_error(!g_panel, "failed to find panel interface");
	
	g_player_info_manager = (IPlayerInfoManager*)tools::get_interface("server.dll", constants::player_info_manager_interface);
	log_error(!g_player_info_manager, "failed to find player info manager interface");

	g_game_event_manager = (IGameEventManager2*)tools::get_interface("engine.dll", constants::game_event_manager);
	log_error(!g_game_event_manager, "failed to find game event manager interface");
	
	g_global_vars = g_player_info_manager->GetGlobalVars();
	log_error(!g_global_vars, "failed to find global vars");


	features::hitmarker::initialize();
	features::menu::initialize();


	log_error(!hooks::install(), "failed to install hooks");

	return(true);
}

bool detach()
{
#ifdef _DEBUG
	tools::detach_console();
#endif

	log_error(!hooks::uninstall(), "failed to uninstall hooks");

	return(true);
}