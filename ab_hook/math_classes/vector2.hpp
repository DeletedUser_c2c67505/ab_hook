#pragma once

class vector2
{
public:
	float x, y;

	vector2() : x(0.f), y(0.f) { }
	vector2(float x, float y) : x(x), y(y) { }

	vector2& operator+(const vector2& v)
	{
		return(vector2(
			x + v.x,
			y + v.y));
	}

	vector2& operator-(const vector2& v)
	{
		return(vector2(
			x - v.x,
			y - v.y));
	}

	vector2& operator+=(const vector2& v)
	{
		x += v.x;
		y += v.y;
		return(*this);
	}

	vector2& operator-=(const vector2& v)
	{
		x -= v.x;
		y -= v.y;
		return(*this);
	}

	vector2& operator*=(float fl)
	{
		x *= fl;
		y *= fl;
		return(*this);
	}

	vector2& operator*=(const vector2& v)
	{
		x *= v.x;
		y *= v.y;
		return(*this);
	}

	vector2& operator/=(const vector2& v)
	{
		x /= v.x;
		y /= v.y;
		return(*this);
	}

	vector2& operator+=(float fl)
	{
		x += fl;
		y += fl;
		return(*this);
	}
	vector2& operator/=(float fl)
	{
		x /= fl;
		y /= fl;
		return(*this);
	}
	vector2& operator-=(float fl)
	{
		x -= fl;
		y -= fl;
		return(*this);
	}
};
