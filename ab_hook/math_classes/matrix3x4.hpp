#pragma once

#include "vector3.hpp"

#include <cmath>

class matrix3x4_t
{
public:
	matrix3x4_t() { }
	matrix3x4_t(
		float m00, float m01, float m02, float m03,
		float m10, float m11, float m12, float m13,
		float m20, float m21, float m22, float m23)
	{
		m_flMatVal[0][0] = m00;
		m_flMatVal[0][1] = m01;
		m_flMatVal[0][2] = m02;
		m_flMatVal[0][3] = m03;

		m_flMatVal[1][0] = m10;
		m_flMatVal[1][1] = m11;
		m_flMatVal[1][2] = m12;
		m_flMatVal[1][3] = m13;

		m_flMatVal[2][0] = m20;
		m_flMatVal[2][1] = m21;
		m_flMatVal[2][2] = m22;
		m_flMatVal[2][3] = m23;
	}

	// Creates a matrix where the X axis = forward
	// the Y axis = left, and the Z axis = up
	void init(const vector3& x_axis, const vector3& y_axis, const vector3& z_axis, const vector3& vec_origin)
	{
		m_flMatVal[0][0] = x_axis.x;
		m_flMatVal[0][1] = y_axis.x;
		m_flMatVal[0][2] = z_axis.x;

		m_flMatVal[1][0] = x_axis.y;
		m_flMatVal[1][1] = y_axis.y; 
		m_flMatVal[1][2] = z_axis.y;

		m_flMatVal[2][0] = x_axis.z;
		m_flMatVal[2][1] = y_axis.z;
		m_flMatVal[2][2] = z_axis.z;

		m_flMatVal[0][3] = vec_origin.x;
		m_flMatVal[1][3] = vec_origin.y;
		m_flMatVal[2][3] = vec_origin.z;
	}

	// Creates a matrix where the X axis = forward
	// the Y axis = left, and the Z axis = up
	matrix3x4_t(const vector3& x_axis, const vector3& y_axis, const vector3& z_axis, const vector3& vec_origin)
	{
		init(x_axis, y_axis, z_axis, vec_origin);
	}

	void set_origin(vector3 const& p)
	{
		m_flMatVal[0][3] = p.x;
		m_flMatVal[1][3] = p.y;
		m_flMatVal[2][3] = p.z;
	}

	void invalidate()
	{
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				m_flMatVal[i][j] = INFINITY;
			}
		}
	}

	float* operator[](int i)
	{
		return m_flMatVal[i];
	}

	const float* operator[](int i) const
	{
		return m_flMatVal[i];
	}
	float* base()
	{
		return &m_flMatVal[0][0];
	}
	const float* base() const
	{
		return &m_flMatVal[0][0];
	}

	float m_flMatVal[3][4];
};