#pragma once

#include <math.h>

class vector3
{
public:
	float x, y, z;

	vector3() : x(0.f), y(0.f), z(0.f) { }
	vector3(float x, float y, float z) : x(x), y(y), z(z) { }

	vector3 operator+(const vector3& v)
	{
		return(vector3(
			x + v.x,
			y + v.y,
			z + v.z));
	}
	vector3 operator-(const vector3& v)
	{
		return(vector3(
			x - v.x,
			y - v.y,
			z - v.z));
	}
	vector3 operator*(const float fl)
	{
		return(vector3(
			x * fl,
			y * fl,
			z * fl));
	}

	vector3& operator+=(const vector3& v)
	{
		x += v.x;
		y += v.y;
		z += v.z;
		return(*this);
	}
	vector3& operator-=(const vector3& v)
	{
		x -= v.x;
		y -= v.y;
		z -= v.z;
		return(*this);
	}
	vector3& operator*=(float fl)
	{
		x *= fl;
		y *= fl;
		z *= fl;
		return(*this);
	}
	vector3& operator*=(const vector3& v)
	{
		x *= v.x;
		y *= v.y;
		z *= v.z;
		return(*this);
	}
	vector3& operator/=(const vector3& v)
	{
		x /= v.x;
		y /= v.y;
		z /= v.z;
		return(*this);
	}
	vector3& operator+=(float fl)
	{
		x += fl;
		y += fl;
		z += fl;
		return(*this);
	}
	vector3& operator/=(float fl)
	{
		x /= fl;
		y /= fl;
		z /= fl;
		return(*this);
	}
	vector3& operator-=(float fl)
	{
		x -= fl;
		y -= fl;
		z -= fl;
		return(*this);
	}

	void clear()
	{
		x = 0.f;
		y = 0.f;
		z = 0.f;
	}
	bool is_valid()
	{
		return(
			!isnan(this->x) &&
			!isnan(this->y) &&
			!isnan(this->z));
	}
};
