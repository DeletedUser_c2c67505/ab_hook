#pragma once 

#include <cmath>

#define VEC_T_NAN *reinterpret_cast<vec_t*>((unsigned long)0x7FC00000)
#define VALVE_RAND_MAX 0x7fff

typedef float vec_t;
class qangle_by_value;

class qangle
{
public:
	// Members
	vec_t x, y, z;

	// Construction/destruction
	qangle(void);
	qangle(vec_t X, vec_t Y, vec_t Z);
	//	qangle(RadianEuler const &angles);	// evil auto type promotion!!!

		// Allow pass-by-value
	operator qangle_by_value &() { return *((qangle_by_value *)(this)); }
	operator const qangle_by_value &() const { return *((const qangle_by_value *)(this)); }

	// Initialization
	void Init(vec_t ix = 0.0f, vec_t iy = 0.0f, vec_t iz = 0.0f);
	void Random(vec_t minVal, vec_t maxVal);

	// Got any nasty NAN's?
	bool IsValid() const;
	void Invalidate();

	// array access...
	vec_t operator[](int i) const;
	vec_t& operator[](int i);

	// Base address...
	vec_t* Base();
	vec_t const* Base() const;

	// equality
	bool operator==(const qangle& v) const;
	bool operator!=(const qangle& v) const;

	// arithmetic operations
	qangle&	operator+=(const qangle &v);
	qangle&	operator-=(const qangle &v);
	qangle&	operator*=(float s);
	qangle&	operator/=(float s);

	// Get the vector's magnitude.
	vec_t	Length() const;
	vec_t	LengthSqr() const;

	// negate the qangle components
	//void	Negate(); 

	// No assignment operators either...
	qangle& operator=(const qangle& src);

	// copy constructors

	// arithmetic operations
	qangle	operator-(void) const;

	qangle	operator+(const qangle& v) const;
	qangle	operator-(const qangle& v) const;
	qangle	operator*(float fl) const;
	qangle	operator/(float fl) const;

private:
	// No copy constructors allowed if we're in optimal mode
	qangle(const qangle& vOther);
};

inline qangle::qangle(void)
{
	x = y = z = VEC_T_NAN;
}

inline qangle::qangle(vec_t X, vec_t Y, vec_t Z)
{
	x = X; y = Y; z = Z;
}

inline void qangle::Init(vec_t ix, vec_t iy, vec_t iz)
{
	x = ix; y = iy; z = iz;
}

inline void qangle::Random(vec_t minVal, vec_t maxVal)
{
	x = minVal + ((float)rand() / VALVE_RAND_MAX) * (maxVal - minVal);
	y = minVal + ((float)rand() / VALVE_RAND_MAX) * (maxVal - minVal);
	z = minVal + ((float)rand() / VALVE_RAND_MAX) * (maxVal - minVal);
}

inline qangle& qangle::operator=(const qangle &vOther)
{
	x = vOther.x; y = vOther.y; z = vOther.z;
	return *this;
}

inline vec_t& qangle::operator[](int i)
{
	return ((vec_t*)this)[i];
}

inline vec_t* qangle::Base()
{
	return (vec_t*)this;
}

inline vec_t const* qangle::Base() const
{
	return (vec_t const*)this;
}

inline bool qangle::IsValid() const
{
	return std::isfinite(x) && std::isfinite(y) && std::isfinite(z);
}

inline void qangle::Invalidate()
{
	x = y = z = VEC_T_NAN;
}

inline bool qangle::operator==(const qangle& src) const
{
	return (src.x == x) && (src.y == y) && (src.z == z);
}

inline bool qangle::operator!=(const qangle& src) const
{
	return (src.x != x) || (src.y != y) || (src.z != z);
}

inline qangle& qangle::operator+=(const qangle& v)
{
	x += v.x; y += v.y; z += v.z;
	return *this;
}

inline qangle& qangle::operator-=(const qangle& v)
{
	x -= v.x; y -= v.y; z -= v.z;
	return *this;
}

inline qangle& qangle::operator*=(float fl)
{
	x *= fl;
	y *= fl;
	z *= fl;
	return *this;
}

inline qangle& qangle::operator/=(float fl)
{
	float oofl = 1.0f / fl;
	x *= oofl;
	y *= oofl;
	z *= oofl;
	return *this;
}

inline vec_t qangle::Length() const
{
	return (vec_t)std::sqrt(LengthSqr());
}


inline vec_t qangle::LengthSqr() const
{
	return x * x + y * y + z * z;
}
inline qangle qangle::operator-(void) const
{
	qangle ret(-x, -y, -z);
	return ret;
}

inline qangle qangle::operator+(const qangle& v) const
{
	qangle res;
	res.x = x + v.x;
	res.y = y + v.y;
	res.z = z + v.z;
	return res;
}

inline qangle qangle::operator-(const qangle& v) const
{
	qangle res;
	res.x = x - v.x;
	res.y = y - v.y;
	res.z = z - v.z;
	return res;
}

inline qangle qangle::operator*(float fl) const
{
	qangle res;
	res.x = x * fl;
	res.y = y * fl;
	res.z = z * fl;
	return res;
}

inline qangle qangle::operator/(float fl) const
{
	qangle res;
	res.x = x / fl;
	res.y = y / fl;
	res.z = z / fl;
	return res;
}

inline vec_t qangle::operator[](int i) const
{
	return ((vec_t*)this)[i];
}
