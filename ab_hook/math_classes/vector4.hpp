#pragma once

class vector4
{
public:
	float x, y, z, w;

	vector4() : x(0.f), y(0.f), z(0.f), w(0.f) { }
	vector4(float x, float y, float z, float w) : x(x), y(y), z(z), w(w) { }

	vector4& operator+(const vector4& v)
	{
		return(vector4(
			x + v.x,
			y + v.y,
			z + v.z,
			w + v.w));
	}

	vector4& operator-(const vector4& v)
	{
		return(vector4(
			x - v.x,
			y - v.y,
			z - v.z,
			w - v.w));
	}

	vector4& operator+=(const vector4& v)
	{
		x += v.x;
		y += v.y;
		z += v.z;
		w += v.w;
		return(*this);
	}

	vector4& operator-=(const vector4& v)
	{
		x -= v.x;
		y -= v.y;
		z -= v.z;
		w -= v.w;
		return(*this);
	}

	vector4& operator*=(float fl)
	{
		x *= fl;
		y *= fl;
		z *= fl;
		w *= fl;
		return(*this);
	}

	vector4& operator*=(const vector4& v)
	{
		x *= v.x;
		y *= v.y;
		z *= v.z;
		w *= v.w;
		return(*this);
	}

	vector4& operator/=(const vector4& v)
	{
		x /= v.x;
		y /= v.y;
		z /= v.z;
		w /= v.w;
		return(*this);
	}

	vector4& operator+=(float fl)
	{
		x += fl;
		y += fl;
		z += fl;
		w += fl;
		return(*this);
	}
	vector4& operator/=(float fl)
	{
		x /= fl;
		y /= fl;
		z /= fl;
		w /= fl;
		return(*this);
	}
	vector4& operator-=(float fl)
	{
		x -= fl;
		y -= fl;
		z -= fl;
		w -= fl;
		return(*this);
	}
};