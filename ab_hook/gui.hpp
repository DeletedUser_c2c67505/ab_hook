#pragma once

#include <unordered_map>
#include <string>

#include "globals.hpp"

using std::unordered_map;
using std::pair;
using std::wstring;

#define BIT_SET(var, bit, val) var = val ? var | bit : var & ~bit

#define BUTTON_CURRENT_STATE 0x1
#define BUTTON_PREVIOUS_STATE 0x2

enum eArrowKey
{
	eArrowKey_Left,
	eArrowKey_Up,
	eArrowKey_Right,
	eArrowKey_Down,
	eArrowKey_Size
};

enum class eElementType
{
	Tab,
	Checkbox,
	Slider,
	Combo
};

struct Element_t
{
public:
	Element_t(eElementType Type, short iHorizontalPos, short iVerticalPos, int iTextLen) :
		m_Type(Type), m_iHorizontalPos(iHorizontalPos), m_iVerticalPos(iVerticalPos), n_iTextLen(iTextLen) { };

	const short m_iHorizontalPos;
	const short m_iVerticalPos;
	const int n_iTextLen;

	//const wstring m_strText;
	const eElementType m_Type;
	bool m_bActive = false;

};

class cGuiInstance
{
public:
	cGuiInstance();
	void Begin(bool bMenuFilling);
	void End();

	bool OnWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

	bool Tab(const wstring strText);
	bool Checkbox(const wstring strText, bool* pBoolean);

	template<typename T>
	bool SliderUniversal(const wstring strText, T* pValue, const wchar_t* szFormat, const T Min, const T Max, T Step);
	bool Combo(const wstring strText, int* pIndex, wstring pStrings[], const int iSize);
	bool SliderInt(const wstring strText, int* pValue, const int iMin, const int iMax, const int iStep = 1)
	{
		return SliderUniversal<int>(strText, pValue, L"%i", iMin, iMax, iStep);
	}
	bool ColorPicker(const wstring text, color_t* color_ptr);


	int m_iPosX = 150;
	int m_iPosY = 50;

	int m_iHorizontalPadding = 115;
	int m_iVerticalPadding = 19;
	int m_iColumnsPadding = 150;

private:
	int GetTextLen(const wstring text)
	{
		const int len = text.size();
		const wchar_t* cstr = text.c_str();

		for (int i = 0; i < len; i++)
			if (cstr[i] == L'#')
				return(i);

		return(len);
	}

	bool WasClicked(eArrowKey key);
	bool IsDown(eArrowKey key);
	void ResetKeyState(eArrowKey key);

	Element_t* UpdateElement(const wstring& strText, const eElementType Type);

	const static int m_iMaxColumns = 8;

	int m_iVerticalIndex[m_iMaxColumns] = { 0 };
	int m_iVerticalBegin[m_iMaxColumns] = { 0 };
	int m_iCurrentVertical[m_iMaxColumns] = { 0 };
	int m_iCurrentHorizontal = 0;

	bool m_bFocused = false;

	int m_pKeysState[eArrowKey_Size] = { 0 };

	unsigned m_Font = 0;

	unordered_map<wstring, Element_t*> m_pElementsList;
};