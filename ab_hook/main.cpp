#include "globals.hpp"
#include "logging.hpp"

BOOL WINAPI DllMain(HMODULE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	if (dwReason == DLL_PROCESS_ATTACH)
	{
		log_info(attach(hInstance) ?
			"inject successful" :
			"inject failed");
	}

	return(TRUE);
}
