#pragma once

/* logs info with [info] prefix */
#define log_info(txt) log_text("[info] %s\n", (txt))
/* if expr, logs error info with [error] prefix and retuns 0 */
#define log_error(expr, txt) if (expr) { log_text("[error] %s | %s(%i)\n", txt, __FILENAME__, __LINE__); return(0); }
/* if expr, logs error code with [error] prefix and retuns 0 */
#define log_error_code(expr, code) if (expr) { log_text("[error] %s(%i) | code: %d\n", __FILENAME__, __LINE__, code); return(0); }

/* prints text to the console */
void log_text(const char* format, ...);
