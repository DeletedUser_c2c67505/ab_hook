#pragma once

#include "sdk/sdk.hpp"


namespace hooks
{
	using draw_model_execute_t = void*(__thiscall*)(void*, const DrawModelState_t*, const ModelRenderInfo_t*, float*);
	extern draw_model_execute_t o_draw_model_execute;

	bool install();
	bool uninstall();
}
