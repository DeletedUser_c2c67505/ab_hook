#include "tools.hpp"

#include <stdio.h>
#include "config.hpp"


#define is_in_range(x, a, b)	(x >= a && x <= b) 
#define get_bits(x)				(is_in_range((x & (~0x20)),'A', 'F') ? ((x & (~0x20)) - 'A' + 0xA) : (is_in_range(x, '0', '9') ? x - '0' : 0))
#define get_byte(x)				(get_bits(x[0]) << 4 | get_bits(x[1]))


#ifdef _WIN64
#define _PTR_MAX_VALUE ((void*)0x000F000000000000)
#else
#define _PTR_MAX_VALUE ((void*)0xFFF00000)
#endif

namespace tools
{
	void attach_console()
	{
		AllocConsole();
		freopen_s((FILE**)stdin, "CONIN$", "r", stdin);
		freopen_s((FILE**)stdout, "CONOUT$", "w", stdout);
		freopen_s((FILE**)stderr, "CONERR$", "w", stderr);

		char buf[512];
		sprintf_s(buf, "%s v%d.%d.%d | built by %s",
			stringify(PROJECT_NAME),
			g_version.major,
			g_version.minor,
			g_version.patch,
			stringify(USER_NAME));

		SetConsoleTitle(buf);

		/* sets green text color */
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
	}

	void detach_console()
	{
		fclose(stdin);
		fclose(stdout);
		fclose(stderr);
		FreeConsole();
	}

	bool module_loaded(const char* name)
	{
		return(GetModuleHandle(name) != 0);
	}

	MODULEINFO get_module_info(const char* name)
	{
		MODULEINFO info = { 0 };

		if (auto module = GetModuleHandle(name))
		{
			static auto current_process = GetCurrentProcess();

			GetModuleInformation(current_process, module, &info, sizeof(info));
		}

		return(info);
	}

	bool is_bad_read_ptr(void* pointer)
	{
		if (!pointer || pointer > _PTR_MAX_VALUE)
			return(false);

		MEMORY_BASIC_INFORMATION mbi = { 0 };

		if (VirtualQuery(pointer, &mbi, sizeof(mbi)))
		{
			if (mbi.Protect & (PAGE_GUARD | PAGE_NOACCESS))
				return(true);

			const unsigned mask = (
				PAGE_READONLY |
				PAGE_READWRITE |
				PAGE_WRITECOPY |
				PAGE_EXECUTE_READ |
				PAGE_EXECUTE_READWRITE |
				PAGE_EXECUTE_WRITECOPY);

			if (mbi.Protect & mask)
				return(false);
		}

		return(true);
	}

	intptr_t scan_pattern(const char* module_name, const char* pattern)
	{
		MODULEINFO info = get_module_info(module_name);

		if (info.lpBaseOfDll)
		{
			const char* current = pattern;

			for (intptr_t find,
				start = (intptr_t)info.lpBaseOfDll,
				pointer = start;
				pointer < (start + (intptr_t)info.SizeOfImage);
				pointer++)
			{
				if (!*current)
					return(find);

				if (*(uint8_t*)current == '\?' ||
					*(uint8_t*)pointer == get_byte(current))
				{
					if (!find)
						find = pointer;

					if (!current[1] || !current[2])
						return(find);

					current += (
						*(uint16_t*)current == '\?\?' ||
						*(uint8_t*)current != '\?')
						? 3 : 2;
				}
				else
				{
					current = pattern;
					find = 0;
				}
			}
		}

		return(0);
	}

	intptr_t scan_pattern(const pattern_info_t pattern_info)
	{
		return(scan_pattern(pattern_info.module_name, pattern_info.pattern) + pattern_info.offset);
	}
	
	void* get_interface(const char* module_name, const char* interface_name)
	{
		if (auto module = GetModuleHandle(module_name))
		{
			using create_interface_t = void*(*)(const char*, int*);

			if (auto create_interface = (create_interface_t)GetProcAddress(module, "CreateInterface"))
			{
				return(create_interface(interface_name, nullptr));
			}
		}

		return(nullptr);
	}
}