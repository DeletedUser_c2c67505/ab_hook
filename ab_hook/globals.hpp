#pragma once

#include <windows.h>

#include "sdk/sdk.hpp"
#include "settings.hpp"
#include "config.hpp"

/* main window handle */
extern HWND g_wnd;
/* current module handle */
extern HMODULE g_module;

/* cheat settings container */
extern settings_t g_settings;

/* interfaces list */
extern IEntityList* g_entity_list;
extern IEngineClient* g_engine_client;
extern IVModelRender* g_model_render;
extern ICvar* g_cvar;
extern IMaterialSystem* g_material_system;
extern IVModelInfo* g_model_info;
extern IVRenderView* g_render_view;
extern ISurface* g_surface;
extern IPanel* g_panel;
extern IPlayerInfoManager* g_player_info_manager;
extern CGlobalVars* g_global_vars;
extern IGameEventManager2* g_game_event_manager;




bool attach(HMODULE current_module);
bool detach();
