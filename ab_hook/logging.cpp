#include "logging.hpp"

#include <cstdarg>
#include <stdio.h>

#include "sdk/utils.hpp"

void log_text(const char* format, ...)
{
	va_list args;
	char buf[1024];
	va_start(args, format);
	vsprintf_s(buf, format, args);
	va_end(args);

#ifdef _DEBUG
	printf(buf);
#else
	con_msg(buf);
#endif
}
