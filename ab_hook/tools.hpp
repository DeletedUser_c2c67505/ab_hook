#pragma once

#include <string.h>
#include <stdint.h>
#include <windows.h>
#include <psapi.h>
#pragma comment(lib, "psapi")


#define stringify_(x) (#x)
/* converts to string */
#define stringify(x) stringify_(x)

/* returns array nodes count */
#define arr_size(arr) (sizeof(arr) / sizeof(*arr))

/* gives current file name */
#define __FILENAME__ (strrchr(__FILE__, '\\') + 1)


namespace tools
{
	struct pattern_info_t
	{
		/* module name with extension */
		char module_name[32];
		/* IDA style pattern */
		char pattern[220];
		/* offset for final pointer */
		intptr_t offset = 0;
	};

	void attach_console();
	void detach_console();

	/* returns true if module is loaded */
	bool module_loaded(const char* name);
	/* returns MODULEINFO struct for module found by name, if module doesn't exist return value is 0 */
	MODULEINFO get_module_info(const char* name);
	/* returns true if pointer is invalid for reading */
	bool is_bad_read_ptr(void* pointer);
	/* returns pointer found by IDA style pattern */
	intptr_t scan_pattern(const char* module_name, const char* pattern);
	intptr_t scan_pattern(const pattern_info_t pattern_info);
	/* returns interface pointer via modules fabric */
	void* get_interface(const char* module_name, const char* interface_name);
}